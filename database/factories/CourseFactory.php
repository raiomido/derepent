<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Raicasts\Models\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'user_id' => 2,
        'category_id' => $faker->randomElement(\App\Raicasts\Models\Category::pluck('id')),
        'title' => ($title = $faker->sentence),
        'slug' => str_slug($title),
        'keywords' => $faker->sentence,
        'overview' => $faker->sentences(6, true),
        'video' => '',
        'duration' => $faker->randomNumber(1).' hrs',
        'price' => ($price = $faker->randomNumber(2)),
        'strike_out_price' => $price - $faker->randomNumber(1),
        'active' => 1,
    ];
});
