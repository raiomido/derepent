<?php

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "description" => $faker->name,
        "slug" => $faker->name,
        "active" => collect(["1","0",])->random(),
        "order" => $faker->randomNumber(2),
    ];
});
