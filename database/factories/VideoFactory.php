<?php

$factory->define(App\Video::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "link" => $faker->name,
        "venue" => $faker->name,
        "date" => $faker->name,
        "description" => $faker->name,
        "order" => $faker->randomNumber(2),
        "show_on_front_page" => collect(["1","0",])->random(),
        "active" => collect(["1","0",])->random(),
    ];
});
