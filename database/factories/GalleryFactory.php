<?php

$factory->define(App\Gallery::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "description" => $faker->name,
        "video_link" => $faker->name,
        "order" => $faker->randomNumber(2),
        "show_on_front_page" => collect(["1","0",])->random(),
        "active" => collect(["1","0",])->random(),
    ];
});
