<?php

$factory->define(App\Document::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "link" => $faker->name,
        "description" => $faker->name,
        "show_on_front_page" => collect(["1","0",])->random(),
        "order" => $faker->randomNumber(2),
        "active" => collect(["1","0",])->random(),
    ];
});
