<?php

use Illuminate\Database\Seeder;

class AnnouncementSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $announcements = [
            [
                'title' => 'Mega International Word Expo',
                'display_from' => now()->format(config('app.date_format')),
                'display_to' => \Carbon\Carbon::createFromFormat(config('app.date_format'), '23-11-2019')->format(config('app.date_format')),
                'description' => 'There will be a mega international word expo at Utawala, Nairobi, Kenya',
                'order' => 1,
                'active' => 1
            ],
        ];

        foreach ($announcements as $announcement) {
            \App\Announcement::create($announcement);
        }
    }
}
