<?php

use Illuminate\Database\Seeder;

class PermissionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'title' => 'user_management_access',],
            ['id' => 2, 'title' => 'permission_access',],
            ['id' => 3, 'title' => 'permission_create',],
            ['id' => 4, 'title' => 'permission_edit',],
            ['id' => 5, 'title' => 'permission_view',],
            ['id' => 6, 'title' => 'permission_delete',],
            ['id' => 7, 'title' => 'role_access',],
            ['id' => 8, 'title' => 'role_create',],
            ['id' => 9, 'title' => 'role_edit',],
            ['id' => 10, 'title' => 'role_view',],
            ['id' => 11, 'title' => 'role_delete',],
            ['id' => 12, 'title' => 'user_access',],
            ['id' => 13, 'title' => 'user_create',],
            ['id' => 14, 'title' => 'user_edit',],
            ['id' => 15, 'title' => 'user_view',],
            ['id' => 16, 'title' => 'user_delete',],
            ['id' => 17, 'title' => 'content_management_access',],
            ['id' => 18, 'title' => 'video_access',],
            ['id' => 19, 'title' => 'video_create',],
            ['id' => 20, 'title' => 'video_edit',],
            ['id' => 21, 'title' => 'video_view',],
            ['id' => 22, 'title' => 'video_delete',],
            ['id' => 23, 'title' => 'category_access',],
            ['id' => 24, 'title' => 'category_create',],
            ['id' => 25, 'title' => 'category_edit',],
            ['id' => 26, 'title' => 'category_view',],
            ['id' => 27, 'title' => 'category_delete',],
            ['id' => 28, 'title' => 'gallery_access',],
            ['id' => 29, 'title' => 'gallery_create',],
            ['id' => 30, 'title' => 'gallery_edit',],
            ['id' => 31, 'title' => 'gallery_view',],
            ['id' => 32, 'title' => 'gallery_delete',],
            ['id' => 33, 'title' => 'document_access',],
            ['id' => 34, 'title' => 'document_create',],
            ['id' => 35, 'title' => 'document_edit',],
            ['id' => 36, 'title' => 'document_view',],
            ['id' => 37, 'title' => 'document_delete',],
            ['id' => 38, 'title' => 'user_action_access',],
            ['id' => 39, 'title' => 'slider_access',],
            ['id' => 40, 'title' => 'slider_create',],
            ['id' => 41, 'title' => 'slider_edit',],
            ['id' => 42, 'title' => 'slider_view',],
            ['id' => 43, 'title' => 'slider_delete',],
            ['id' => 44, 'title' => 'announcement_access',],
            ['id' => 45, 'title' => 'announcement_create',],
            ['id' => 46, 'title' => 'announcement_edit',],
            ['id' => 47, 'title' => 'announcement_view',],
            ['id' => 48, 'title' => 'announcement_delete',],
            ['id' => 49, 'title' => 'page_access',],
            ['id' => 50, 'title' => 'page_create',],
            ['id' => 51, 'title' => 'page_edit',],
            ['id' => 52, 'title' => 'page_view',],
            ['id' => 53, 'title' => 'page_delete',],

        ];

        foreach ($items as $item) {
            \App\Permission::create($item);
        }
    }
}