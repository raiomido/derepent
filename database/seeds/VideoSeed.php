<?php

use Illuminate\Database\Seeder;

class VideoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $videos = [
            [
                'title' => 'Prophezeiung vom 9.12.2017',
                'link' => 'https://www.youtube.com/watch?v=ESxOd7piiqM',
                'categories' => [1,3],
                'description' => 'Prophezeiung 9.12.2017 über ein schweres erdbeben, welches nach Deutschland kommt - PROPHET DR. OWUOR',
                'order' => 1,
                'show_on_front_page' => 1,
                'major_vision' => 0,
                'fulfilled_prophecy' => 0,
                'active' => 1,
            ],
            [
                'title' => 'Prophezeiung vom 9.12.2017',
                'link' => 'https://www.youtube.com/watch?v=-nzpmfPmMhM',
                'categories' => [1,3],
                'description' => 'Prophezeiung 9.12.2017 über ein schweres erdbeben, welches nach Deutschland kommt',
                'order' => 2,
                'show_on_front_page' => 1,
                'major_vision' => 0,
                'fulfilled_prophecy' => 0,
                'active' => 1,
            ],
            [
                'title' => 'Prophezeiung vom 16.11.2017',
                'link' => 'https://www.youtube.com/watch?v=b1PizvHfzOo',
                'categories' => [1,3],
                'description' => 'Prophezeiung vom 16.11.2017 über dem sturm, der nach Deutschland kommt - PROPHET DR. OWUOR',
                'order' => 3,
                'show_on_front_page' => 1,
                'major_vision' => 0,
                'fulfilled_prophecy' => 0,
                'active' => 1,
            ],
            [
                'title' => 'Prophezeiung vom 05.11.2017',
                'link' => 'https://www.youtube.com/watch?v=AuDudmr_mZM',
                'categories' => [1,3],
                'description' => '05.11.2017 Prophezeiung über das urteil welches nach Deutschland und zu dem golf von Mexiko kommt',
                'order' => 4,
                'show_on_front_page' => 1,
                'major_vision' => 0,
                'fulfilled_prophecy' => 0,
                'active' => 1,
            ],
            [
                'title' => 'Prophezeiung über historische überflutungen, die auf die erde kommen',
                'link' => 'https://www.youtube.com/watch?v=V6ZpQWqKvqs',
                'categories' => [1,3],
                'description' => 'Prophezeiung über historische überflutungen, die auf die erde kommen',
                'order' => 5,
                'show_on_front_page' => 1,
                'major_vision' => 0,
                'fulfilled_prophecy' => 0,
                'active' => 1,
            ],
            [
                'title' => '1.03.2019 Der Segen Von Den MÄCHTIGSTEN PROPHETEN VON JEHOVA JAHWE',
                'link' => 'https://www.youtube.com/watch?v=4LqUZMDjn5M',
                'categories' => [1,3],
                'description' => '1.03.2019 Der Segen Von Den MÄCHTIGSTEN PROPHETEN VON JEHOVA JAHWE',
                'order' => 6,
                'show_on_front_page' => 1,
                'major_vision' => 0,
                'fulfilled_prophecy' => 0,
                'active' => 1,
            ],
            [
                'title' => 'DIE VISION VON DEM ANTICHRISTEN',
                'link' => 'https://www.youtube.com/watch?v=R_N5Z5dtb_E',
                'categories' => [1,3],
                'description' => 'DIE VISION VON DEM ANTICHRISTEN',
                'order' => 7,
                'show_on_front_page' => 1,
                'major_vision' => 0,
                'fulfilled_prophecy' => 0,
                'active' => 1,
            ],
            [
                'title' => 'DIE VISION VON DEM ANTICHRISTEN',
                'link' => 'https://www.youtube.com/watch?v=R_N5Z5dtb_E',
                'categories' => [1,3],
                'description' => 'DIE VISION VON DEM ANTICHRISTEN',
                'order' => 7,
                'show_on_front_page' => 1,
                'major_vision' => 0,
                'fulfilled_prophecy' => 0,
                'active' => 1,
            ],
            [
                'title' => 'Prophezeiungen',
                'link' => 'https://www.youtube.com/playlist?list=PLcqMSNUDS7S1IIi8va9unW_gqWN8qt1iX',
                'categories' => [3],
                'description' => 'Prophezeiungen',
                'order' => 8,
                'show_on_front_page' => 1,
                'major_vision' => 0,
                'fulfilled_prophecy' => 0,
                'active' => 1,
            ],
        ];

        foreach ($videos as $video) {
            $image = 'public/images/dummy/healing-germany.jpg';
            $cats = $video['categories'];
            unset($video['categories']);
            $svideo = \App\Video::create($video);
            $svideo->categories()->sync($cats);

            $svideo->copyMedia(new \Illuminate\Http\File($image))->toMediaCollection('image');
        }
    }
}