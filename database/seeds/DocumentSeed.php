<?php

use Illuminate\Database\Seeder;

class DocumentSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $documents = [
            [
                'title' => 'Prophezeiung vom 16.11.2017',
                'link' => '',
                'file' => 'prophezeiung-vom-16.11.2017',
                'description' => '',
                'show_on_front_page' => 1,
                'order' => 1,
                'active' => 1
            ],
            [
                'title' => 'Prophezeiung vom 9.12.2017 Deutschland',
                'link' => '',
                'file' => 'prophezeiung-vom-9.12.2017-deutschland',
                'description' => '',
                'show_on_front_page' => 1,
                'order' => 2,
                'active' => 1
            ],
            [
                'title' => 'Prophezeiung 5.11.2017',
                'link' => '',
                'file' => 'prophezeiung-5.11.2017',
                'description' => '',
                'show_on_front_page' => 1,
                'order' => 3,
                'active' => 1
            ],
        ];

        foreach ($documents as $document) {
            $image = 'public/images/dummy/documents/prophecy-germany.jpg';
            $file = "public/images/dummy/documents/{$document['file']}.pdf";
            $sdocument = \App\Document::create($document);
            $sdocument->copyMedia(new \Illuminate\Http\File($image))->toMediaCollection('image');
            $sdocument->copyMedia(new \Illuminate\Http\File($file))->toMediaCollection('file');
        }
    }
}
