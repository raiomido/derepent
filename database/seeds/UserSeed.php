<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$users = [
			[
				'name' => 'Rai Omido',
				'email' => 'raiomido@gmail.com',
				'password' => bcrypt('@milkyway2019'),
				'active' => 1
			],
            [
				'name' => 'Bishop Julia Bruchwitz',
				'email' => 'juliabruchwitzberlin@gmail.com',
				'password' => bcrypt('@milkyway2019'),
				'active' => 1
			],
		];

		foreach ($users as $user) {
			\App\User::create($user);
		}
	}
}
