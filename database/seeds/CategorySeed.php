<?php

use Illuminate\Database\Seeder;

class CategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'title' => 'Prophecies about Germany',
                'slug' => 'prophecies-about-germany',
                'image' => '',
                'order' => 0,
                'active' => 1,
            ],
            [
                'title' => 'Teachings',
                'slug' => 'teachings',
                'image' => '',
                'order' => 0,
                'active' => 1,
            ],
            [
                'title' => 'Prophecies',
                'slug' => 'prophecies',
                'image' => '',
                'order' => 0,
                'active' => 1,
            ],
            [
                'title' => 'Revival',
                'slug' => 'revival',
                'image' => '',
                'order' => 0,
                'active' => 1,
            ],
            [
                'title' => 'Documentaries',
                'slug' => 'documentaries',
                'image' => '',
                'order' => 0,
                'active' => 1,
            ],
        ];

        foreach ($categories as $category) {
            \App\Category::create($category);
        }
    }
}
