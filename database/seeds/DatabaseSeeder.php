<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$this->call(PermissionSeed::class);
		$this->call(RoleSeed::class);
		$this->call(UserSeed::class);
		$this->call(RoleSeedPivot::class);
        $this->call(UserSeedPivot::class);
        $this->call(SliderSeed::class);
        $this->call(CategorySeed::class);
        $this->call(DocumentSeed::class);
        $this->call(VideoSeed::class);
//        $this->call(AnnouncementSeed::class);
    }
}
