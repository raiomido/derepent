<?php

use Illuminate\Database\Seeder;

class SliderSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = \Illuminate\Support\Facades\File::allFiles(public_path('/images/dummy/slides'));
        $fileSys = new \Illuminate\Filesystem\Filesystem();
        $fileSys->deleteDirectories(storage_path('app/public'));

//        $gallery = \App\Gallery::create([
//            'title' => __("The Messiah is Coming"),
//            'description' => '',
//            'video_link' => '#',
//            'active' => 1,
//            'order' => 1
//        ]);

        foreach ($images as $index => $image) {
            $sslide = \App\Slider::create([
                'title' => '',
                'description' => '',
                'video_link' => '#',
                'active' => 1,
                'order' => 1
            ]);
            $sslide->copyMedia(new \Illuminate\Http\File($images[$index]))->toMediaCollection('image');
//            $gallery->copyMedia(new \Illuminate\Http\File($images[$index]))->withResponsiveImages()->toMediaCollection('images');
        }

    }
}
