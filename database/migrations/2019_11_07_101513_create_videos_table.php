<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('link');
            $table->string('country')->nullable();
            $table->string('venue')->nullable();
            $table->string('date')->nullable();
            $table->text('description')->nullable();
            $table->integer('order')->nullable();
            $table->string('show_on_front_page')->nullable();
            $table->string('major_vision')->nullable();
            $table->string('fulfilled_prophecy')->nullable();
            $table->string('active')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index(['deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
