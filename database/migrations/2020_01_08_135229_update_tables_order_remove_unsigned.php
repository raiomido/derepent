<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTablesOrderRemoveUnsigned extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->integer('order')->change();
        });
        Schema::table('videos', function (Blueprint $table) {
            $table->integer('order')->change();
        });
        Schema::table('galleries', function (Blueprint $table) {
            $table->integer('order')->change();
        });
        Schema::table('documents', function (Blueprint $table) {
            $table->integer('order')->change();
        });
        Schema::table('sliders', function (Blueprint $table) {
            $table->integer('order')->change();
        });
        Schema::table('announcements', function (Blueprint $table) {
            $table->integer('order')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
