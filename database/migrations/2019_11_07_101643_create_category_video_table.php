<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_video', function (Blueprint $table) {
            $table->bigInteger('category_id')->unsigned()->nullable();
            $table->foreign('category_id', 'fk_p_42307_42306_video_ca_5dc3eb7d350fa')->references('id')->on('categories')->onDelete('cascade');
            $table->bigInteger('video_id')->unsigned()->nullable();
            $table->foreign('video_id', 'fk_p_42306_42307_category_5dc3eb7d35141')->references('id')->on('videos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_video');
    }
}
