<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.index')}}" aria-expanded="false">
                        <i class="mdi mdi-av-timer"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                @if($user->can('user_management_access'))
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="mdi mdi-account-circle"></i>
                            <span class="hide-menu">@lang('site.user-management.title')</span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            @if($user->can('user_access'))
                            <li class="sidebar-item">
                                <router-link :to="{ name: 'users.index' }" class="sidebar-link">
                                    <i class="mdi mdi-account-network"></i>
                                    <span class="hide-menu">@lang('site.users.title')</span>
                                </router-link>
                            </li>
                            @endif
                            @if($user->can('permission_access'))
                            <li class="sidebar-item">
                                <router-link :to="{ name: 'permissions.index' }" class="sidebar-link">
                                    <i class="mdi mdi-lock"></i>
                                    <span class="hide-menu">@lang('site.permissions.title')</span>
                                </router-link>
                            </li>
                            @endif
                            @if($user->can('role_access'))
                            <li class="sidebar-item">
                                <router-link :to="{ name: 'roles.index' }" class="sidebar-link">
                                    <i class="mdi mdi-account-key"></i>
                                    <span class="hide-menu">@lang('site.roles.title')</span>
                                </router-link>
                            </li>
                            @endif
                            @if($user->can('user_action_access'))
                            <li class="sidebar-item">
                                <router-link :to="{ name: 'user_actions.index' }" class="sidebar-link">
                                    <i class="mdi mdi-account-check"></i>
                                    <span class="hide-menu">@lang('site.user-actions.title')</span>
                                </router-link>
                            </li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if($user->can('content_management_access'))
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="mdi mdi-file-document"></i>
                            <span class="hide-menu">@lang('site.content-management.title')</span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            @if($user->can('user_access'))
                            <li class="sidebar-item">
                                <router-link :to="{ name: 'categories.index' }" class="sidebar-link">
                                    <i class="mdi mdi-briefcase"></i>
                                    <span class="hide-menu">@lang('site.categories.title')</span>
                                </router-link>
                            </li>
                            @endif
                            @if($user->can('video_access'))
                            <li class="sidebar-item">
                                <router-link :to="{ name: 'videos.index' }" class="sidebar-link">
                                    <i class="mdi mdi-play-circle"></i>
                                    <span class="hide-menu">@lang('site.videos.title')</span>
                                </router-link>
                            </li>
                            @endif
                            @if($user->can('gallery_access'))
                            <li class="sidebar-item">
                                <router-link :to="{ name: 'galleries.index' }" class="sidebar-link">
                                    <i class="mdi mdi-image-album"></i>
                                    <span class="hide-menu">@lang('site.galleries.title')</span>
                                </router-link>
                            </li>
                            @endif
                            @if($user->can('document_access'))
                            <li class="sidebar-item">
                                <router-link :to="{ name: 'documents.index' }" class="sidebar-link">
                                    <i class="mdi mdi-file-pdf"></i>
                                    <span class="hide-menu">@lang('site.documents.title')</span>
                                </router-link>
                            </li>
                            @endif
                            @if($user->can('slider_access'))
                            <li class="sidebar-item">
                                <router-link :to="{ name: 'sliders.index' }" class="sidebar-link">
                                    <i class="mdi mdi-image-area"></i>
                                    <span class="hide-menu">@lang('site.slider.title')</span>
                                </router-link>
                            </li>
                            @endif
                            @if($user->can('announcement_access'))
                            <li class="sidebar-item">
                                <router-link :to="{ name: 'announcements.index' }" class="sidebar-link">
                                    <i class="mdi mdi-calendar-clock"></i>
                                    <span class="hide-menu">@lang('site.announcements.title')</span>
                                </router-link>
                            </li>
                            @endif
                            @if($user->can('nugget_access'))
                                <li class="sidebar-item">
                                    <router-link :to="{ name: 'nuggets.index' }" class="sidebar-link">
                                        <i class="mdi mdi-image-album"></i>
                                        <span class="hide-menu">@lang('site.nuggets.title')</span>
                                    </router-link>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
                <li class="sidebar-item">
                    <router-link :to="{ name: 'auth.change_password' }" class="sidebar-link waves-effect waves-dark sidebar-link">
                        <i class="mdi mdi-security"></i>
                        <span class="hide-menu">@lang('site.de_change_password')</span>
                    </router-link>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" onclick="event.preventDefault(); $('#logout').submit();" href="{{route('logout')}}" aria-expanded="false">
                        <i class="mdi mdi-power"></i>
                        <span class="hide-menu">@lang('site.de_logout')</span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
