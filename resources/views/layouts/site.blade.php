<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="Rai Omido">
    <meta name="description" content="{{$pageDescription ?? $naturalPageDescription}}">
    <meta name="keywords" content="{{$pageKeywords ?? $naturalPageKeywords}}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">

    <title>{{$pageTitle ?? $naturalPageTitle}} - {{config('app.name')}}</title>


    <meta property="og:title" content="{{$pageTitle ?? $naturalPageTitle}} - {{env('APP_NAME')}}">
    <meta property="og:description" content="{{$pageDescription ?? $naturalPageDescription}}">
    <meta property="og:image" content="{{$ogImage ?? $naturalOgImage}}">

    <link rel="apple-touch-icon" href="{{ asset('vendor/bootstrap/assets/images/apple-touch-icon.png') }}">
    <link rel="shortcut icon" href="{{ asset('/favicon.ico ') }}">

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/fancybox/css/jquery.fancybox.css') }}">

    <link rel="stylesheet" href="{{ asset('/vendor/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/owl-carousel/dist/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/vendor/owl-carousel/dist/assets/owl.theme.default.min.css')}}">

    <link rel="stylesheet" href="{{ asset('css/derepent.css') }}">

    <script src='https://www.google.com/recaptcha/api.js?hl=de'></script>
    @stack('styles')
</head>
<body>

{{--<div class="container">--}}
<div id="wrapper">
    @include('layouts.common.header')
    <div class="container">
        <div class="content">
            @yield('content')
        </div>
        @include('layouts.common.footer')
    </div>
</div>
{{--</div>--}}
<!-- The Modal end -->
</body>
<script src="{{ asset('vendor/jquery/js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('/vendor/popper/popper.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{url('js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('/vendor/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('/vendor/masonry/masonry.pkgd.min.js')}}"></script>
<script src="{{ asset('/vendor/fancybox/js/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset('vendor/modernizr/js/modernizr.js') }}"></script>
<script src="{{asset('/vendor/owl-carousel/dist/owl.carousel.min.js')}}"></script>
<script src="{{asset('/js/derepent.js')}}"></script>
<script>
    $(window).on("load", function (e) {
        // Animate loader off screen
        $(".rc-loader").fadeOut("slow");
    });
</script>
@yield('javascript')
@stack('scripts')
</html>
