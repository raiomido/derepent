<!doctype html>
<html class="no-js css-menubar" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="dp-date" content="{{ config('app.date_format_moment') }}">
    <meta name="dp-time" content="{{ config('app.time_format_moment') }}">
    <meta name="dp-datetime" content="{{ config('app.datetime_format_moment') }}">
    <meta name="app-locale" content="{{ App::getLocale() }}">

    <title>{{$pageTitle ?? $naturalPageTitle}} | {{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/back.css') }}" rel="stylesheet">
</head>
<body>
<div class="main-wrapper">
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center">
    <div class="auth-box">
        <div id="loginform">
            <div class="logo">
                <a href="{{route('home')}}" class="d-block mb-3"><img src="{{url('images/icons/logo.png')}}" width="36" alt="logo"></a>
                <h5 class="font-medium mb-3">{{__($pageTitle ?? $naturalPageTitle)}}</h5>
            </div>
            <!-- Form -->
            <div class="row">
                <div class="col-12">
                <div class="form-horizontal m-t-20" id="loginform">
                    @yield('content')
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src="{{url('/vendor/jquery/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{url('/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('/vendor/popper/popper.min.js')}}"></script>
<script src="{{url('/vendor/sparkline/sparkline.js')}}"></script>
<script src="{{url('/js/back.js')}}"></script>
</body>
</html>