<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                &copy; {{now()->year}}&nbsp;{{$website_info->impressum}}&emsp;|&emsp;{{$website_info->email}}&emsp;|&emsp;{{$website_info->phone}}
            </div>
        </div>
    </div>
</footer>
