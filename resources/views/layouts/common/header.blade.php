@php
    $hide = $hide_header ?? false;
@endphp
@unless($hide)
    <header>
        <div class="top-bar">
            <div class="container h-100">
                <div class="d-flex align-items-center justify-content-center justify-content-md-between h-100">
                    <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                        <a href="{{url('/')}}">
                            <img src="{{url('images/icons/logo.png')}}" width="60" alt="{{config('app.name')}}">
                        </a>
                        <h2 class="headline text-white mb-0 mx-3">
                            {{config('app.name')}}
                        </h2>
                    </div>
                    @if($live_video)
                        <div class="px-3">
                            <a href="{{route('stream.live')}}" class="bg-danger text-white rounded px-3 py-2 d-flex align-items-center">
                                <i class="fa fa-video mr-2"></i> <span class="d-none d-md-flex">Live</span>
                            </a>
                        </div>
                    @endif
                    <p class="tagline text-white mb-0">
                        {{__('Prepare the way')}}<br>
                        {{__('The Messiah is Coming')}}<br>
                        {{__('Revelation 16:15')}}
                    </p>
                </div>
            </div>
        </div>
    </header>
    <nav class="navbar navbar-expand-md navbar-dark main-menu text-uppercase">
        <div class="container">
            <a href="#" class="navbar-brand d-block d-md-none"></a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <div class="navbar-nav">
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle {{is_active(['home', '/', 'statement-of-faith'])}}"
                           data-toggle="dropdown">{{__('Home')}}</a>
                        <div class="dropdown-menu">
                            <a href="{{url('/')}}" class="dropdown-item">{{__('Home')}}</a>
                            <a href="{{route('statement-of-faith')}}" class="dropdown-item ">{{__('Statement of Faith')}}</a>
                            <a href="{{config('sys.general.links.prophetic_calling')}}" target="_blank"
                               class="dropdown-item">{{__('Prophetic Calling')}}</a>
                        </div>
                    </div>
                    <a href="{{config('sys.general.links.main_website')}}" target="_blank"
                       class="nav-item nav-link">{{__('English Website')}}</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle {{is_active(['prophecies', 'prophecies/fulfilled', 'categories/prophecies-about-germany'])}}" data-toggle="dropdown">{{__('Prophecies')}}</a>
                        <div class="dropdown-menu">
                            <a href="{{route('categories.show', 'prophecies-about-germany')}}" class="dropdown-item">{{__('Prophecies about Germany')}}</a>
                            <a href="{{route('prophecies')}}" class="dropdown-item">{{__('Prophecy Alert')}}</a>
                            <a href="{{route('fulfilled-prophecies')}}" class="dropdown-item">{{__('Fulfilled Prophecies')}}</a>
                        </div>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle {{is_active(['categories/teachings'])}}" data-toggle="dropdown">{{__('Teachings')}}</a>
                        <div class="dropdown-menu">
                            <a href="{{route('categories.show', 'teachings')}}"
                               class="dropdown-item">{{__('Video Teachings')}}</a>
                            <a href="{{config('sys.general.links.audio')}}" target="_blank"
                               class="dropdown-item">{{__('Audio Teachings')}}</a>
                        </div>
                    </div>
                    <a href="{{route('galleries.index')}}" class="nav-item nav-link {{is_active('galleries')}}">{{__('Gallery')}}</a>
                    <a href="{{route('salvation-prayer')}}"
                       class="nav-item nav-link {{is_active('salvation-prayer')}}">{{__('Salvation Prayer')}}</a>
                    <a href="{{route('fellowships')}}"
                       class="nav-item nav-link {{is_active('fellowships')}}">{{__('Fellowships')}}</a>
                    <a href="{{route('contacts')}}" class="nav-item nav-link {{is_active('contacts')}}">{{__('Contacts')}}</a>

                </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="smart-marquee">
            <span>{{__('site.isaiah_40_3')}}</span>
        </div>
    </div>
@endunless
