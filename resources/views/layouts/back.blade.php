<!doctype html>
<html class="no-js css-menubar" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="dp-date" content="{{ config('app.date_format_moment') }}">
    <meta name="dp-time" content="{{ config('app.time_format_moment') }}">
    <meta name="dp-datetime" content="{{ config('app.datetime_format_moment') }}">
    <meta name="app-locale" content="{{ App::getLocale() }}">

    <title>{{$pageTitle ?? $naturalPageTitle}} | {{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/back.css') }}" rel="stylesheet">
</head>
<body>
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="app">
<div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full" >
    <header class="topbar" data-navbarbg="skin6">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <div class="navbar-header" data-logobg="skin5">
                <!-- This is for the sidebar toggle which is visible on mobile only -->
                <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                    <i class="ti-menu ti-close"></i>
                </a>
                <div class="navbar-brand">
                    <a href="{{url('/')}}" class="logo">
                        <img src="{{url('images/icons/logo.png')}}" alt="{{config('app.name')}}" width="40">
                    </a>
                </div>
                <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent"
                   aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="ti-more"></i>
                </a>
            </div>
            <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin6">
                <ul class="navbar-nav float-left mr-auto">
                </ul>
                <ul class="navbar-nav float-right">

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{$user->name}}
                            {!! $user->avatar_link !!}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right user-dd animated border-0">
                            <span class="with-arrow">
                                    <span class="bg-primary"></span>
                                </span>
                            <div class="d-flex no-block align-items-center p-3 bg-primary text-white m-b-10">
                                <div class="mr-3">
                                    {!! $user->avatar_link_large !!}
                                </div>
                                <div class="m-l-10">
                                    <h4 class="m-b-0">{{$user->name}}</h4>
                                    <p class=" m-b-0">{{$user->email}}</p>
                                </div>
                            </div>
                            <a class="dropdown-item" onclick="event.preventDefault(); $('#logout').submit();" href="{{route('logout')}}"><i class="ti-power-off m-r-5 m-l-5"></i> @lang('site.de_logout')</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    @include('backoffice.partials.sidebar')
    <div class="page-wrapper">
        <div class="container-fluid">
            <event-hub></event-hub>
            <router-view></router-view>
        </div>
        <footer class="footer text-center">
            &copy; {{now()->year}} | {{config('app.name')}}
        </footer>
    </div>
    <form method="post" id="logout" action="{{route('logout')}}" class="d-none">
        @csrf
    </form>
</div>
</div>
<script src="{{url('/vendor/jquery/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{url('/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('/vendor/popper/popper.min.js')}}"></script>
{{-- CK Editor --}}
<script src="https://cdn.ckeditor.com/4.9.2/full/ckeditor.js"></script>
<script src="{{url('/vendor/sparkline/sparkline.js')}}"></script>
<script src="{{url('/js/back.js')}}"></script>

{{-- Vue --}}
<script src="{{ mix('/client/js/manifest.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ mix('/client/js/vendor.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ mix('/client/js/app.js') }}" type="text/javascript" charset="utf-8"></script>

</body>
</html>
