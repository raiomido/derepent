@extends('layouts.site')
@section('content')
    <section class="error-page h-100">
        <div class="container p-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="card basic-page bg-theme h-100">
                        <div class="card-body pb-3">
                            <div class="row align-items-center" style="height: 420px">
                               <div class="col-md-12 text-center">
                                   <h1 class="mb-3">{{__('Sorry')}}</h1>
                                   <div class="mb-3">
                                       <i class="fa fa-sad-tear error-emoji"></i>
                                   </div>
                                   <h4>{{__('That content was not found on this website')}}</h4>
                               </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection