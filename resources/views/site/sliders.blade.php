@extends('layouts.site')
@section('content')
    <section>
     <div class="container p-0">
         <div class="row">
             <div class="col-md-12">
                 <div class="card no-bg glorious-blue mb-4 basic-page">
                     <div class="card-header">
                         <h4 class="card-title text-uppercase mb-0">{{__($pageTitle ?? $naturalPageTitle)}}</h4>
                     </div>

                     <div class="card-body">
                         <div class="gallery-wrapper masonable-wrapper grid row no-gutters">
                             <div class="col-md-3 grid-sizer"></div>
                             @foreach($sliders as $image)
                                 <a class="grid-item col-md-3" href="{{url($image->image_url)}}" data-fancybox="grid-gallery" data-caption="{{$image->title}}">
                                     <div class="image-container">
                                         <div class="overlay"></div>
                                         <img class="shadow" src="{{url($image->image_url)}}">
                                         <i class="fa fa-camera icon"></i>
                                     </div>
                                 </a>
                             @endforeach
                         </div>
                         <div class="mt-4">
                             {{$sliders->links()}}
                         </div>
                     </div>
                     </div>
                 </div>
             </div>
         </div>
    </section>
@endsection
