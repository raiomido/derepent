@extends('layouts.site')
@section('content')
    <section>
        <div class="container p-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="card basic-page">
                        <div class="card-header">
                            <h4 class="card-title text-uppercase mb-0">{{__($pageTitle)}}</h4>
                        </div>
                        <div class="card-body pb-3">
                            <div class="row">
                                @foreach($items as $item)
                                <div class="col-md-6 mb-4">
                                    <div class="callout callout-theme h-100">
                                        <h4 class="text-yellow">{{$item['name']}}</h4>
                                        <p>{!! $item['street'] !!}</p>
                                        <p>{!! $item['city_town_area'] !!}</p>
                                        <p>{!! $item['further_directions'] !!}</p>
                                        @isset($item['phone'])
                                        <p class="font-weight-bold text-orange">{{__("Phone")}}:&nbsp;{{$item['phone']}}</p>
                                        @endisset
                                        @isset($item['time'])
                                        <p class="font-weight-bold text-orange">{{$item['time']}}</p>
                                        @endisset
                                    </div>
                                </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection