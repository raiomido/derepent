@extends('layouts.site')
@section('content')
    <section>
     <div class="container p-0">
         <div class="row">
             <div class="col-md-12">
                 <div class="card no-bg glorious-blue mb-4 basic-page">
                     <div class="card-header">
                         <h4 class="card-title text-uppercase mb-0">{{__($pageTitle ?? $naturalPageTitle)}}</h4>
                     </div>
                     <div class="card-body pt-3 pb-0 px-2">
                         <div class="row no-gutters">
                             @foreach($videos as $video)
                                 <div class="col-md-6">
                                     <div class="card mx-2 mb-3">
                                         <div class="card-body p-0">
                                             <div class="item p-3 border shadow-sm">
                                                 <a class="title mb-0 d-block popup-video" href="{{$video->link}}">
                                                     <div class="row no-gutters flex-column flex-md-row align-items-center">
                                                         <div class="col-md-6 mb-3 mb-md-0">
                                                             <iframe width="100%" src="{{$video->embed_link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                         </div>
                                                         <div class="col-md-6 link-text pl-3">
                                                             {{$video->title}}
                                                         </div>
                                                     </div>
                                                 </a>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             @endforeach
                             <div class="col-md-12">
                                 <div class="px-2">
                                     {{$videos->links()}}
                                 </div>
                             </div>
                         </div>
                     </div>


                 </div>
             </div>
         </div>
     </div>
    </section>
@endsection
