@extends('layouts.site')
@section('content')
    <section>
        <div class="container p-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="card no-bg glorious-blue mb-4 basic-page">
                        <div class="card-header">
                            <h4 class="card-title text-uppercase mb-0"><a class="text-yellow" href="{{route('nuggets.index')}}">{{__('Nuggets')}}</a>&emsp;<i class="fa fa-angle-double-right"></i>&emsp;{{__($pageTitle ?? $naturalPageTitle)}}</h4>
                        </div>

                        <div class="card-body">
                            <div class="gallery-wrapper masonable-wrapper grid row">
                                <div class="col-md-6 p-2 grid-sizer"></div>
                                @foreach($nugget_images as $image)
                                    @php
                                        $url = $image->getUrl()
                                    @endphp
                                    <a class="grid-item col-md-6 p-2" href="{{url($url)}}" data-fancybox="grid-gallery" data-caption="{{$image->title}}">
                                        <div class="image-container">
                                            <div class="overlay"></div>
                                            <img class="shadow" src="{{url($url)}}">
                                            <i class="fa fa-camera icon"></i>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                            <div class="mt-4">
                                {{$nugget_images->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
