@extends('layouts.site')
@section('content')
    <section>
     <div class="container p-0">
         <div class="row">
             <div class="col-md-12">
                 <div class="card basic-page bg-white vh-100">
                     <div class="card-header">
                         <h4 class="card-title text-uppercase mb-0">{{__($pageTitle ?? $naturalPageTitle)}}</h4>
                     </div>
                     <div class="card-body p-3">
                        <div class="tags">
                            @foreach($categories as $category)
                                <a target="_blank" href="{{route('categories.show', $category)}}"><span>{{__($category->title)}}</span></a>
                            @endforeach
                        </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
    </section>
@endsection
