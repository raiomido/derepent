@extends('layouts.site')
@section('content')
    <section>
        <div class="smart-marquee">
            <span>{{__($live_video->title ?? $naturalPageTitle)}}</span>
        </div>
        <div class="container h-100 p-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="card glorious-blue border-0">
                        <div class="card-header">
                            <div class="card-title text-uppercase mb-0">
                                <a href="{{route('home')}}" class="d-flex w-100 align-items-center justify-content-center text-yellow py-2">
                                    <img src="{{url('images/icons/logo.png')}}" width="40" height="40" alt="{{config('app.name')}}">
                                </a>
                            </div>
                        </div>
                        <div class="card-body p-0 glorious-blue">
                            <div class="item">
                                <div class="w-100 position-relative" style="padding-bottom: 56%">
                                    @if($live_video)
                                        <iframe class="position-absolute w-100 h-100" width="100%" src="{{$live_video->embed_link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    @else
                                        <div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center text-white">
                                            <div class="text-center">
                                                <i class="fa fa-video-slash error-emoji mb-3"></i>
                                                <h3 class="mb-4">Livestream is off</h3>
                                                <h5><a class="d-flex align-items-center justify-content-center" href="{{route('stream.live')}}"><i class="fa fa-undo text-yellow"></i><small class="text-yellow">&nbsp;refresh</small></a></h5>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
    <style>

    </style>
@endpush
