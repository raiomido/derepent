@extends('layouts.site')
@section('content')
    <section>
     <div class="container p-0">
         <div class="row">
             <div class="col-md-12">
                 <div class="card basic-page">
                     <div class="card-header">
                         <h4 class="card-title text-uppercase mb-0">{{__($pageTitle)}}</h4>
                     </div>
                     <div class="card-body">
                         <div class="callout callout-theme">
                             <p class="highlight">{{__('site.salvation_prayer.body')}}</p>
                             <p class="text-yellow font-weight-bold">{{__('site.salvation_prayer.conclusion')}}</p>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
    </section>
@endsection