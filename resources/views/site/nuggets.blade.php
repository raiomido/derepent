@extends('layouts.site')
@section('content')
    <section>
     <div class="container p-0">
                    <div class="card no-bg glorious-blue basic-page">
                     <div class="card-header">
                         <h4 class="card-title text-uppercase mb-0">{{__($pageTitle ?? $naturalPageTitle)}}</h4>
                     </div>
                     <div class="card-body pb-0 pt-3 px-3">
                         <div class="row">
                             @foreach($nuggets as $nugget)
                                 <div class="col-md-6 mb-3 {{$loop->odd ? 'pr-md-0' : ''}}">
                                     <div class="card home-gallery-carousel-wrapper">
                                         <div class="card-header d-flex justify-content-between align-items-center">
                                             <h4 class="card-title text-uppercase mb-0">{{$nugget->title}}</h4>
                                             <small><a class="text-yellow mr-2" href="{{route('nuggets.show', $nugget)}}">{{__('See all')}}&nbsp;<i class="fa fa-angle-right"></i></a> </small>
                                         </div>
                                         <div class="card-body p-0">
                                             <div class="py-3">
                                                 @if($nugget->latest_images)
                                                     <div class="home-gallery-carousel owl-carousel owl-theme">
                                                         @foreach($nugget->latest_images as $image)
                                                             @php
                                                                 $url = $image->getUrl();
                                                             @endphp
                                                             <a class="item" href="{{url($url)}}" data-fancybox="grid-gallery" data-caption="{{$nugget->title}}">
                                                                 <div class="d-flex justify-content-center">
                                                                     <img style="max-height: 300px;" class="shadow" src="{{url($url)}}">
                                                                 </div>
                                                             </a>
                                                         @endforeach
                                                     </div>
                                                 @endif
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             @endforeach
                             <div class="col-md-12">
                                 <div class="px-2">
                                     {{$nuggets->links()}}
                                 </div>
                             </div>
                         </div>
                     </div>


                 </div>
             </div>
    </section>
@endsection
