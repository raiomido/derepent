@extends('layouts.site')
@section('content')
    <section>
        <div class="container p-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="card basic-page">
                        <div class="card-header">
                            <h4 class="card-title text-uppercase mb-0">{{__($pageTitle)}}</h4>
                        </div>
                        <div class="card-body pb-3">
                            <div class="row">
                                @foreach($items as $item)
                                <div class="col-md-6 mb-4">
                                    <div class="callout callout-theme h-100">
                                        <h4 class="text-yellow">{{__('site.statement_of_faith.'.$item.'.title')}}</h4>
                                        <p>{{__('site.statement_of_faith.'.$item.'.body')}}</p>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection