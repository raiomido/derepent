@extends('layouts.site')
@section('content')
    @php
        $saturday = \Illuminate\Support\Carbon::createFromDate('2021', '03', '20', 'Africa/Nairobi');
        $isHealingService = now()->isBefore($saturday);
    @endphp
    <section>
        <div>
            <div id="homeCarousel" class="owl-carousel owl-theme">
                @foreach($sliders as $item)
                    <div class="item">
                            <div class="d-flex align-items-center justify-content-{{$item->title ? 'between' : 'center'}}">
                                <div>
                                    {{$item->image}}
                                </div>
                                @if($item->title)
                                <div>
                                    <h1 class="title">{{$item->title}}</h1>
                                    <p class="caption">{{strip_tags($item->description)}}</p>
                                </div>
                                @endif
                            </div>
                        </div>
                @endforeach
                <div class="item">
                    <div class="d-flex align-items-center justify-content-center">
                        <a class="btn btn-theme btn-lg" href="{{route('slider-images.index')}}">{{__('More Images')}} <i class="fa fa-angle-double-right"></i> </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if($announcements->count())
    <div class="alert alert-info fade show alert-dismissible border-0 mb-0 mt-3" role="alert">
        <div id="announcementsCarousel" class="owl-carousel owl-theme">
            @foreach($announcements as $announcement)
                <div class="item">
                    <h5 class="mb-0 font-weight-bold">{{$announcement->title}}</h5> {!! $announcement->description !!}
                </div>
            @endforeach
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <section class="section pb-0">
        <div class="container-fluid px-0 px-md-3">
        <div class="row">
            <div class="col-md-6 pr-md-0 order-1 order-md-0">
                <div class="card mb-3 documents">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h4 class="card-title text-uppercase mb-0 no-wrap">{{__('Documents')}}</h4>
                    </div>
                    <div class="card-body p-0" style="height: 200px;">
                        <div id="documentsCarousel" class="owl-carousel owl-theme">
                            @foreach($documents as $doc)
                                <div class="mb-2 item px-2 py-3 shadow">
                                    <div class="container">
                                        <div class="row align-items-center justify-content-center">
                                            <div class="col">
                                                <h1 class="title">{{$doc->title}}</h1>
                                                <a class="download-btn px-2 py-1 no-wrap" href="{{$doc->file_link ? $doc->file_link : $doc->link}}">{{__("Download PDF")}}</a>
                                            </div>
                                            @if($doc->image)
                                                <div class="col d-flex justify-content-center">
                                                    {{$doc->image}}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                    <div class="card blue-widget mb-3">
                        <a href="https://www.youtube.com/JESUSKOMMTDeutschland/live" class="card-body d-flex justify-content-center">
                            <img src="{{url('images/invitation-general.jpeg')}}" class="h-485" />
                        </a>
                    </div>
            </div>
            <div class="col-md-6  order-0 order-md-1">
                @if($isHealingService)
                    @include('partials.single-video', [
                           'link' => 'https://www.youtube.com/embed/bZe7XGeC5sE'
                       ])
                @endif
                <div class="card glorious-blue mb-3">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h4 class="card-title text-uppercase mb-0 no-wrap">{{__("Listen to the End Time Radio")}}</h4>
                    </div>
                    <div class="card-body p-0" style="height: 200px;">
                        <div class="item">
                            <iframe src="https://mixlr.com/endzeitradio/embed" width="100%" height="180px" scrolling="no" frameborder="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                    </div>
                </div>
                <div class="card glorious-blue mb-3">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h4 class="card-title text-uppercase mb-0 no-wrap">{{__("Jesus Is Lord Radio")}}</h4>
                        <small><a class="text-yellow mr-2 no-wrap" target="_blank" href="{{config('sys.general.links.radio')}}">{{__('Visit the Website')}}&nbsp;<i class="fa fa-angle-right"></i></a> </small>
                    </div>
                    <div class="card-body p-0">
                        <div class="item">
                            <div id="jilr" class="w-100 d-flex align-items-center justify-content-center"></div>
                        </div>
                    </div>
                </div>
                @if(!$isHealingService && $promo)
                    @include('partials.single-video', [
                           'link' => $promo->embed_link
                       ])
                @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-md-0 order-1 order-md-0">
                @if($prophecies_about_germany->count())
            <div class="card prophecies-about-germany glorious-blue mb-3 border-0">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h4 class="card-title text-uppercase mb-0 no-wrap">{{__('Prophecies about Germany')}}</h4>
                    <small><a class="text-yellow mr-2 no-wrap" href="{{route('categories.show', 'prophecies-about-germany')}}">{{__('See all')}}&nbsp;<i class="fa fa-angle-right"></i></a> </small>
                </div>
            <div class="card-body p-0">
                <div class="overflow">
                @foreach($prophecies_about_germany as $prof)
                    <div class="item p-3">
                        <div class="container">
                            <div class="row py-3 align-items-md-center justify-content-md-between flex-column flex-md-row">
                                <div class="col-12 col-md-4 d-flex justify-content-center mb-3 mb-md-0">
                                    {{$prof->image}}
                                </div>
                                <div class="col-12 col-md-8">
                                    <a class="title d-block mb-1 popup-video" href="{{$prof->link}}">{{$prof->title}}</a>
                                    <p class="caption mb-0">{{strip_tags($prof->description)}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
                </div>
                </div>
                @endif
                @if($fulfilled_prophecies->count())
                    <div class="card fulfilled-prophecies blue-widget mb-3">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title text-uppercase mb-0 no-wrap">{{__('Accurately Fulfilled Prophecies')}}</h4>
                            <small><a class="text-yellow mr-2 no-wrap" href="{{route('fulfilled-prophecies')}}">{{__('See all')}}&nbsp;<i class="fa fa-angle-right"></i></a> </small>
                        </div>
                        <div class="card-body p-0">
                            <div class="overflow">
                                @foreach($fulfilled_prophecies as $fp)
                                    <div class="item px-2 pt-3">
                                        <a class="title d-block popup-video" href="{{$fp->link}}">{{$fp->title}}</a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
                @if($prophecies->count())
                <div class="card prophecies mb-3">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h4 class="card-title text-uppercase mb-0 no-wrap">{{__('Prophecy Alert')}}</h4>
                        <small><a class="text-yellow mr-2 no-wrap" href="{{route('prophecies')}}">{{__('See all')}}&nbsp;<i class="fa fa-angle-right"></i></a> </small>
                    </div>
                    <div class="card-body p-0">
                        <div class="overflow mb-3">
                            @foreach($prophecies as $prophecy)
                                <div class="item px-2 pt-3">
                                    <div class="container">
                                        <div class="row align-items-center justify-content-center">
                                            <div class="col">
                                                <a class="title d-block popup-video" href="{{$prophecy->link}}">{{$prophecy->title}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="col-md-6  order-0 order-md-1">
                @if($recent_prophecies->count())
                    <div class="card de-list text-yellow mb-3">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title text-uppercase mb-0 no-wrap">{{__('Recent Prophecies')}}</h4>
                            <small><a class="text-yellow mr-2 no-wrap" href="{{route('categories.show', 'recent-prophecies')}}">{{__('See all')}}&nbsp;<i class="fa fa-angle-right"></i></a> </small>
                        </div>
                        <div class="card-body p-0">
                            <div class="overflow ">
                                @foreach($recent_prophecies as $r_prophecies)
                                    <div class="item p-2">
                                        <a class="title mb-0 p-2 d-block popup-video" href="{{$r_prophecies->link}}">{{$r_prophecies->title}}</a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
                @if($major_visions->count())
                <div class="card glorious-orange mb-3">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h4 class="card-title text-uppercase mb-0 no-wrap">{{__('Major Visions of THE LORD')}}</h4>
                    </div>
                    <div class="card-body p-0">
                        <div id="visitationsCarousel" class="owl-carousel owl-theme">
                            @foreach($major_visions as $vision)
                                <div class="mb-2 item px-2 py-3 shadow">
                                    <div class="container">
                                        <div class="row align-items-center justify-content-center">
                                            <div class="col">
                                                <h1 class="title">{{$vision->title}}</h1>
                                                <p class="caption mb-0"><a class="popup-video" href="{{$vision->link}}" target="_blank">{{__("Watch")}} <i class="fa fa-angle-double-right"></i> </a> </p>
                                            </div>
                                            <div class="col d-flex justify-content-center">
                                                {{$vision->image}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
                @if($general_videos && $general_videos->count())
                <div class="card glorious-blue-2 small-carousel mb-3">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h4 class="card-title text-uppercase mb-0 no-wrap">{{__('Videos')}}</h4>
                        <small><a class="text-yellow mr-2 no-wrap" href="{{route('categories.show', 'general')}}">{{__('See all')}}&nbsp;<i class="fa fa-angle-right"></i></a> </small>
                    </div>
                    <div class="card-body p-0">
                        <div id="generalVideosCarousel" class="owl-carousel owl-theme">
                            @foreach($general_videos as $general_video)
                                <div class="mb-2 item px-2 py-3 shadow">
                                    <div class="container">
                                        <div class="row align-items-center justify-content-center">
                                            <div class="col">
                                                <h1 class="title">{{$general_video->title}}</h1>
                                                <p class="caption mb-0"><a class="popup-video" href="{{$general_video->link}}" target="_blank">{{__("Watch")}} <i class="fa fa-angle-double-right"></i> </a> </p>
                                            </div>
                                            <div class="col d-flex justify-content-center">
                                                {{$general_video->image}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
                @if($nuggets && $nuggets->count())
                    <div class="card home-gallery-carousel-wrapper mb-3">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title text-uppercase mb-0 no-wrap">{{$nuggets->title}}</h4>
                            <small><a class="text-yellow mr-2 no-wrap" href="{{route('nuggets.index')}}">{{__('More Nuggets')}}&nbsp;<i class="fa fa-angle-right"></i></a> </small>
                        </div>
                        <div class="card-body p-0">
                            <div class="py-3">
                                @if($nuggets->latest_images)
                                    <div class="home-gallery-carousel owl-carousel owl-theme">
                                        @foreach($nuggets->latest_images as $image)
                                            @php
                                                $url = $image->getUrl();
                                            @endphp
                                            <a class="item" href="{{url($url)}}" data-fancybox="grid-gallery" data-caption="{{$nuggets->title}}">
                                                <div class="d-flex justify-content-center">
                                                    <img class="shadow" src="{{url($url)}}">
                                                </div>
                                            </a>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif
                @if($revival->count())
                    <div class="card de-list mb-3 revival">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title text-uppercase mb-0 no-wrap">{{__('Tremendous Global Revival')}}</h4>
                            <small><a class="text-yellow mr-2 no-wrap" href="{{route('categories.show', 'revival')}}">{{__('See all')}}&nbsp;<i class="fa fa-angle-right"></i></a> </small>
                        </div>
                        <div class="card-body p-0">
                            <div class="overflow ">
                                @foreach($revival as $rev)
                                    <div class="item p-2">
                                        <a class="title mb-0 p-2 d-block popup-video" href="{{$rev->link}}">{{$rev->title}}</a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
                </div>
            </div>
            @if($galleries)
            <div class="row">
                @foreach($galleries as $gallery)
                <div class="col-md-6 mb-3 {{$loop->odd ? 'pr-md-0' : ''}}">
                    <div class="card home-gallery-carousel-wrapper">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title text-uppercase mb-0 no-wrap">{{$gallery->title}}</h4>
                            <small><a class="text-yellow mr-2 no-wrap" href="{{route('galleries.index')}}">{{__('Gallery')}}&nbsp;<i class="fa fa-angle-right"></i></a> </small>
                        </div>
                        <div class="card-lead">
                            <p class="card-title mb-0">{{$gallery->description}}</p>
                        </div>
                        <div class="card-body p-0">
                            <div class="py-3">
                                @if($gallery->images)
                                <div class="home-gallery-carousel owl-carousel owl-theme">
                                    @foreach($gallery->images as $image)
                                        @php
                                            $url = $image->getUrl();
                                        @endphp
                                        <a class="item" href="{{url($url)}}" data-fancybox="grid-gallery" data-caption="{{$gallery->title}}">
                                            <div class="d-flex justify-content-center">
                                                <img class="shadow" src="{{url($url)}}">
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>
                    @endforeach
            </div>
            @endif
            <div class="row">
                <div class="col-md-6 mb-3 pr-md-0">
                    <div class="card audio-teachings-link py-4">
                        <div class="card-body p-0">
                            <div class="item p-2">
                                <a class="title mb-0 p-2 d-block" href="{{config('sys.general.links.audio')}}">{{__('Audio Teachings')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mb-3">
                    <div class="card galleries-link py-4">
                        <div class="card-body p-0">
                            <div class="item p-2">
                                <a class="title mb-0 p-2 d-block" href="{{url('galleries')}}">{{__('Photo Galleries')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @foreach($other_categories as $cat)
                @php
                $title = __($cat->title);
                @endphp
            <div class="card no-bg glorious-blue mb-4">
                <div class="card-header">
                    <h4 class="card-title text-uppercase mb-0 no-wrap">{{$title}}</h4>
                </div>
                <div class="card-body pt-3 p-0">
                    <div class="row">
                        @foreach($cat->videos as $c_video)
                            <div class="col-md-6 {{$loop->odd ? 'pr-md-0' : ''}}">
                                <div class="card mb-3">
                                <div class="card-body p-0">
                                    <div class="item p-3 border shadow-sm">
                                        <a class="title mb-0 d-block popup-video" href="{{$c_video->link}}">
                                            <div class="row flex-column flex-md-row align-items-center">
                                                <div class="col-md-6 mb-3 mb-md-0">
                                                    <iframe width="100%" src="{{$c_video->embed_link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                </div>
                                                <div class="col-md-6 link-text pl-3">
                                                    {{$c_video->title}}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                        <div class="col-md-12">
                            <div class="px-2">
                                <a class="font-weight-bold text-red" href="{{route('categories.show', $cat)}}">{{__('See all')}}&nbsp;<i class="fa fa-angle-right"></i> </a>
                            </div>
                        </div>
                </div>
                </div>

            </div>
            @endforeach
        <div class="row">
            <div class="col-md-6 pr-md-0">
                <div class="card radio-link mb-3 border-0">
                    <div class="card-body p-0">
                        <div class="item p-2">
                            <a class="title mb-0 p-2 d-block" href="{{config('sys.general.links.radio')}}"><div class="d-flex justify-content-between"><div class="col-8 link-text pl-3 py-1">{{__('Listen to the Radio')}}</div> <div class="col-4 jil-icon">&nbsp;</div></div></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card youtube-link mb-3 p-0 border-0">
                    <div class="card-body p-0">
                        <div class="item">
                            <a class="title mb-0 d-block" href="{{config('sys.general.links.youtube')}}">
                                <div class="d-flex justify-content-between">
                                    <div class="col-8 link-text pl-3 py-3 d-flex align-items-center">{{__('Visit us on youtube')}}</div>
                                    <div class="col-4 link-icon-wrapper py-3">
                                        <div class="link-icon"></div>
                                    </div>
                                </div> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection

@push('styles')
    <style>
        .h-485 {
            max-height: 485px;
        }
        .w-auto {
            width: auto;
        }
        #jilr {
            height: 180px;
        }
        .revival.card .card-body .overflow {
            height: 425px;
            max-height: 425px;
        }
        .promo.card .card-body .overflow {
            max-height: 294px;
        }
        .card.fulfilled-prophecies .card-body .overflow {
            max-height: 465px;
            min-height: 465px;
        }
        .card.prophecies .card-body .overflow {
            max-height: 567px;
        }
    </style>
@endpush
@push('scripts')
    <!-- BEGINS: AUTO-GENERATED MUSES RADIO PLAYER CODE -->
    <!-- BEGINS: AUTO-GENERATED MUSES RADIO PLAYER CODE -->
    <script type="text/javascript" src="https://hosted.muses.org/mrp.js"></script>
    <script type="text/javascript">
        MRP.insert({
            'url':'https://s3.radio.co/s97f38db97/listen',
            'lang':'en',
            'codec':'mp3',
            'volume':50,
            'autoplay':false,
            'forceHTML5':true,
            'jsevents':true,
            'buffering':0,
            'title':'Jesus Is Lord Radio',
            'wmode':'transparent',
            'skin':'faredirfare',
            'width':269,
            'height':52,
            'elementId': 'jilr'
        });
    </script>
@endpush
