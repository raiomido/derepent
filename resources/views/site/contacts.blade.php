@extends('layouts.site')
@section('content')
    <section>
        <div class="container p-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="card contacts-page basic-page">
                        <div class="card-header">
                            <h4 class="card-title text-uppercase mb-0">{{__($pageTitle)}}</h4>
                        </div>
                        <div class="card-body my-5">
                            @include('partials.flash-messages')
                            <div class="row justify-content-center">
                                <div class="col-md-7">
                                    <div class="card p-3 shadow">
                                    <div class="card-body">
                                        <form method="post" action="{{route('contacts.email')}}" class="mt-4">
                                            @csrf
                                            <div class="form-group">
                                                <input name="name" type="text"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       placeholder="{{__('Name')}}" value="{{ old('name') }}">
                                                @error('name')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <input name="email" type="email"
                                                       class="form-control @error('email') is-invalid @enderror"
                                                       placeholder="{{__('Email')}}" value="{{ old('email') }}">
                                                @error('email')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <input name="subject" type="text"
                                                       class="form-control @error('subject') is-invalid @enderror"
                                                       placeholder="{{__('Subject')}}" value="{{ old('subject') }}">
                                                @error('subject')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <textarea name="message" rows="7"
                                                          class="form-control @error('message') is-invalid @enderror"
                                                          placeholder="{{__('Message')}}">{{ old('message') }}</textarea>
                                                @error('message')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="mb-4">
                                                <div class="g-recaptcha"
                                                     data-sitekey="{{config('sys.settings.recaptcha.site')}}"></div>
                                                <div class="@error('g-recaptcha-response')is-invalid  @enderror">
                                                    @error('g-recaptcha-response')
                                                    <span class="invalid-feedback d-block">
                                                {{$message}}
                                            </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-theme btn-lg btn-block"
                                                        type="submit">{{__('Send Message')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
