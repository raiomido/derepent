@extends('layouts.site')
@section('content')
    <section>
     <div class="container p-0">
         <div class="row">
             <div class="col-md-12">
                 <div class="card galleries basic-page border-0 bg-theme">
                     <div class="card-header">
                         <h4 class="card-title text-uppercase mb-0">{{__($pageTitle)}}</h4>
                     </div>
                     <div class="card-body">
                         <div class="row">
                             <div class="col-md-12">
                                 @foreach($galleries as $gallery)
                                     @if($gallery->images)
                                         <div class="item pb-3">
                                         <header><h4 class="text-yellow p-0">{{$gallery->title}}</h4></header>
                                         <p>{{$gallery->description}}</p>
                                         <div class="gallery-wrapper masonable-wrapper grid row no-gutters">
                                             <div class="col-md-3 grid-sizer"></div>
                                             @foreach($gallery->images as $image)
                                                 @php
                                                     $url = $image->getUrl();
                                                 @endphp
                                                 <a class="grid-item col-md-3" href="{{url($url)}}" data-fancybox="grid-gallery" data-caption="{{$gallery->title}}">
                                                     <div class="image-container">
                                                         <div class="overlay"></div>
                                                         <img class="shadow" src="{{url($url)}}">
                                                         <i class="fa fa-camera icon"></i>
                                                     </div>
                                                 </a>
                                             @endforeach
                                         </div>
                                         </div>
                                         <div class="be-border w-100 my-5"></div>
                                     @endif
                                 @endforeach
                             </div>
                             <div class="col-md-12 mt-4">
                                 {{$galleries->links()}}
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
    </section>
@endsection