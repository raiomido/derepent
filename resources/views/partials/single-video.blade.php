<div class="card glorious-blue promo mb-3">
    <div class="card-body p-0">
        <div class="item">
            <div class="w-100 position-relative" style="padding-bottom: 56%">
                <iframe class="position-absolute w-100 h-100" width="100%"
                        src="{{$link}}"
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
