import Vue from 'vue'
import Vuex from 'vuex'
import Alert from './modules/alert'
import ChangePassword from './modules/change_password'
import Paginator from './modules/paginator'
import Rules from './modules/rules'
import PermissionsIndex from './modules/Permissions'
import PermissionsSingle from './modules/Permissions/single'
import RolesIndex from './modules/Roles'
import RolesSingle from './modules/Roles/single'
import UsersIndex from './modules/Users'
import UsersSingle from './modules/Users/single'
import CategoriesIndex from './modules/Categories'
import CategoriesSingle from './modules/Categories/single'
import VideosIndex from './modules/Videos'
import VideosSingle from './modules/Videos/single'
import GalleriesIndex from './modules/Galleries'
import GalleriesSingle from './modules/Galleries/single'
import DocumentsIndex from './modules/Documents'
import DocumentsSingle from './modules/Documents/single'
import UserActionsIndex from './modules/UserActions'
import SlidersIndex from './modules/Sliders'
import SlidersSingle from './modules/Sliders/single'
import AnnouncementsIndex from './modules/Announcements'
import AnnouncementsSingle from './modules/Announcements/single'
import NuggetsIndex from './modules/Nuggets'
import NuggetsSingle from './modules/Nuggets/single'
import MediaSingle from './modules/Media/single'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        Alert,
        ChangePassword,
        Paginator,
        Rules,
        PermissionsIndex,
        PermissionsSingle,
        RolesIndex,
        RolesSingle,
        UsersIndex,
        UsersSingle,
        CategoriesIndex,
        CategoriesSingle,
        VideosIndex,
        VideosSingle,
        GalleriesIndex,
        GalleriesSingle,
        DocumentsIndex,
        DocumentsSingle,
        UserActionsIndex,
        NuggetsIndex,
        NuggetsSingle,
        AnnouncementsIndex,
        AnnouncementsSingle,
        SlidersIndex,
        SlidersSingle,
        MediaSingle,
    },
    strict: debug,
})
