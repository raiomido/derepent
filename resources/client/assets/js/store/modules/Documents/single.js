function initialState() {
    return {
        item: {
            id: null,
            title: null,
            link: null,
            file: null,
            image: null,
            description: null,
            show_on_front_page: null,
            order: null,
            active: null,
        },
        
        
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (state.item.file === null) {
                params.delete('file');
            }
            if (state.item.image === null) {
                params.delete('image');
            }

            axios.post('/api/v1/documents', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (state.item.file === null) {
                params.delete('file');
            }
            if (state.item.image === null) {
                params.delete('image');
            }

            axios.post('/api/v1/documents/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/documents/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        
    },
    
    setTitle({ commit }, value) {
        commit('setTitle', value)
    },
    setLink({ commit }, value) {
        commit('setLink', value)
    },
    setFile({ commit }, value) {
        commit('setFile', value)
    },
    
    setImage({ commit }, value) {
        commit('setImage', value)
    },
    
    setDescription({ commit }, value) {
        commit('setDescription', value)
    },
    setShow_on_front_page({ commit }, value) {
        commit('setShow_on_front_page', value)
    },
    setOrder({ commit }, value) {
        commit('setOrder', value)
    },
    setActive({ commit }, value) {
        commit('setActive', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setTitle(state, value) {
        state.item.title = value
    },
    setLink(state, value) {
        state.item.link = value
    },
    setFile(state, value) {
        state.item.file = value
    },
    setImage(state, value) {
        state.item.image = value
    },
    setDescription(state, value) {
        state.item.description = value
    },
    setShow_on_front_page(state, value) {
        state.item.show_on_front_page = value
    },
    setOrder(state, value) {
        state.item.order = value
    },
    setActive(state, value) {
        state.item.active = value
    },
    
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
