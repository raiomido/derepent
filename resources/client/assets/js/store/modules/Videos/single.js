function initialState() {
    return {
        item: {
            id: null,
            title: null,
            link: null,
            categories: [],
            image: null,
            country: null,
            venue: null,
            date: null,
            description: null,
            order: null,
            major_vision: null,
            fulfilled_prophecy: null,
            show_on_front_page: null,
            active: null,
        },
        categoriesAll: [],
        
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    loading: state => state.loading,
    categoriesAll: state => state.categoriesAll,
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.categories)) {
                params.delete('categories')
            } else {
                for (let index in state.item.categories) {
                    params.set('categories['+index+']', state.item.categories[index].id)
                }
            }
            if (state.item.image === null) {
                params.delete('image');
            }

            axios.post('/api/v1/videos', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            if (_.isEmpty(state.item.categories)) {
                params.delete('categories')
            } else {
                for (let index in state.item.categories) {
                    params.set('categories['+index+']', state.item.categories[index].id)
                }
            }
            if (state.item.image === null) {
                params.delete('image');
            }

            axios.post('/api/v1/videos/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/videos/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        dispatch('fetchCategoriesAll')
    },
    fetchCategoriesAll({ commit }) {
        axios.get('/api/v1/categories')
            .then(response => {
                commit('setCategoriesAll', response.data.data)
            })
    },
    setTitle({ commit }, value) {
        commit('setTitle', value)
    },
    setLink({ commit }, value) {
        commit('setLink', value)
    },
    setCategories({ commit }, value) {
        commit('setCategories', value)
    },
    setImage({ commit }, value) {
        commit('setImage', value)
    },
    
    setCountry({ commit }, value) {
        commit('setCountry', value)
    },
    setVenue({ commit }, value) {
        commit('setVenue', value)
    },
    setDate({ commit }, value) {
        commit('setDate', value)
    },
    setDescription({ commit }, value) {
        commit('setDescription', value)
    },
    setOrder({ commit }, value) {
        commit('setOrder', value)
    },
    setMajor_vision({ commit }, value) {
        commit('setMajor_vision', value)
    },
    setFulfilled_prophecy({ commit }, value) {
        commit('setFulfilled_prophecy', value)
    },
    setShow_on_front_page({ commit }, value) {
        commit('setShow_on_front_page', value)
    },
    setActive({ commit }, value) {
        commit('setActive', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setTitle(state, value) {
        state.item.title = value
    },
    setLink(state, value) {
        state.item.link = value
    },
    setCategories(state, value) {
        state.item.categories = value
    },
    setImage(state, value) {
        state.item.image = value
    },
    setCountry(state, value) {
        state.item.country = value
    },
    setVenue(state, value) {
        state.item.venue = value
    },
    setDate(state, value) {
        state.item.date = value
    },
    setDescription(state, value) {
        state.item.description = value
    },
    setOrder(state, value) {
        state.item.order = value
    },
    setMajor_vision(state, value) {
        state.item.major_vision = value
    },
    setFulfilled_prophecy(state, value) {
        state.item.fulfilled_prophecy = value
    },
    setShow_on_front_page(state, value) {
        state.item.show_on_front_page = value
    },
    setActive(state, value) {
        state.item.active = value
    },
    setCategoriesAll(state, value) {
        state.categoriesAll = value
    },
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
