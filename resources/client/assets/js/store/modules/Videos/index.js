function initialState() {
    return {
        all: [],
        paginationData: [],
        relationships: {
            'categories': 'title',
        },
        query: {},
        loading: false
    }
}

const getters = {
    data: state => {
        let rows = state.all

        if (state.query.sort) {
            rows = _.orderBy(state.all, state.query.sort, state.query.order)
        }

        return rows.slice(state.query.offset, state.query.offset + state.query.limit)
    },
    total:         state => state.all.length,
    paginationData:         state => state.paginationData,
    loading:       state => state.loading,
    relationships: state => state.relationships
}

const actions = {
    fetchData({ commit, dispatch }, query) {
        commit('setLoading', true)

        let searchParams = new URLSearchParams()

        for (let fieldName in query) {
            let fieldValue = query[fieldName];
            if (typeof fieldValue !== 'object') {
                searchParams.set(fieldName, fieldValue);
            } else {
                if (fieldValue && typeof fieldValue[0] !== 'object') {
                    searchParams.set(fieldName, fieldValue);
                } else {
                    for (let index in fieldValue) {
                        searchParams.set(fieldName + '[' + index + ']', fieldValue[index]);
                    }
                }
            }
        }
        axios.get('/api/v1/videos',  {params: searchParams})
            .then(response => {
                commit('setAll', response.data.data)
                commit('setPagination', response.data)
            })
            .catch(error => {
                let message = error.response.data.message || error.message
                let errors = error.response.data.errors
                dispatch(
                    'Alert/setAlert',
                    {message: message, errors: errors, color: 'danger'},
                    {root: true})
            })
            .finally(() => {
                commit('setLoading', false)
            })
    },
    destroyData({ commit, state, dispatch }, id) {
        axios.delete('/api/v1/videos/' + id)
            .then(response => {
                commit('setAll', state.all.filter((item) => {
                    return item.id != id
                }))
            })
            .catch(error => {
                let message = error.response.data.message || error.message
                let errors = error.response.data.errors

                dispatch(
                    'Alert/setAlert',
                    {message: message, errors: errors, color: 'danger'},
                    {root: true})
            })
    },
    setQuery({ commit }, value) {
        commit('setQuery', purify(value))
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setAll(state, items) {
        state.all = items
    },
    setPagination(state, data) {
        state.paginationData = data
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    setQuery(state, query) {
        state.query = query
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
