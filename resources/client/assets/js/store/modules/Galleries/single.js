function initialState() {
    return {
        item: {
            id: null,
            title: null,
            description: null,
            images: [],
            uploaded_images: [],
            video_link: null,
            order: null,
            show_on_front_page: null,
            active: null,
        },

        image_urls:[],
        loading: false,
    }
}

const getters = {
    item: state => state.item,
    image_urls: state => state.image_urls,
    loading: state => state.loading,
    
    
}

const actions = {
    storeData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            params.set('uploaded_images', state.item.uploaded_images.map(o => o['id']))

            axios.post('/api/v1/galleries', params)
                .then(response => {
                    commit('resetState')
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    updateData({ commit, state, dispatch }) {
        commit('setLoading', true)
        dispatch('Alert/resetState', null, { root: true })

        return new Promise((resolve, reject) => {
            let params = new FormData();
            params.set('_method', 'PUT')

            for (let fieldName in state.item) {
                let fieldValue = state.item[fieldName];
                if (typeof fieldValue !== 'object') {
                    params.set(fieldName, fieldValue);
                } else {
                    if (fieldValue && typeof fieldValue[0] !== 'object') {
                        params.set(fieldName, fieldValue);
                    } else {
                        for (let index in fieldValue) {
                            params.set(fieldName + '[' + index + ']', fieldValue[index]);
                        }
                    }
                }
            }

            params.set('uploaded_images', state.item.uploaded_images.map(o => o['id']))

            axios.post('/api/v1/galleries/' + state.item.id, params)
                .then(response => {
                    commit('setItem', response.data.data)
                    resolve()
                })
                .catch(error => {
                    let message = error.response.data.message || error.message
                    let errors  = error.response.data.errors

                    dispatch(
                        'Alert/setAlert',
                        { message: message, errors: errors, color: 'danger' },
                        { root: true })

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },
    fetchData({ commit, dispatch }, id) {
        axios.get('/api/v1/galleries/' + id)
            .then(response => {
                commit('setItem', response.data.data)
            })

        
    },
    
    setTitle({ commit }, value) {
        commit('setTitle', value)
    },
    setDescription({ commit }, value) {
        commit('setDescription', value)
    },
    setImages({ commit }, value) {
        commit('setImages', value)
    },
    setImageUrls({ commit }, value) {
        commit('setImageUrls', value)
    },
    destroyImages({ commit }, value) {
        commit('destroyImages', value)
    },
    destroyUploadedImages({ commit }, value) {
        commit('destroyUploadedImages', value)
    },
    setVideo_link({ commit }, value) {
        commit('setVideo_link', value)
    },
    setOrder({ commit }, value) {
        commit('setOrder', value)
    },
    setShow_on_front_page({ commit }, value) {
        commit('setShow_on_front_page', value)
    },
    setActive({ commit }, value) {
        commit('setActive', value)
    },
    resetState({ commit }) {
        commit('resetState')
    }
}

const mutations = {
    setItem(state, item) {
        state.item = item
    },
    setTitle(state, value) {
        state.item.title = value
    },
    setDescription(state, value) {
        state.item.description = value
    },
    setImages(state, value) {
        for (let i in value) {
            let images = value[i];
            if (typeof images === "object") {
                state.item.images.push(images);
            }
        }
    },
    setImageUrls(state, value) {
        state.image_urls.push(value);
    },
    destroyImages(state, value) {
        for (let i in state.item.images) {
            if (i == value) {
                state.item.images.splice(i, 1);
            }
        }

        for (let i in state.image_urls) {
            const subject = _.find(state.image_urls, { index: value });
            if(subject) {
                _.remove(state.image_urls, subject)
            }
        }
    },
    destroyUploadedImages(state, value) {
        for (let i in state.item.uploaded_images) {
            let data = state.item.uploaded_images[i];
            if (data.id === value) {
                state.item.uploaded_images.splice(i, 1);
            }
        }
    },
    setVideo_link(state, value) {
        state.item.video_link = value
    },
    setOrder(state, value) {
        state.item.order = value
    },
    setShow_on_front_page(state, value) {
        state.item.show_on_front_page = value
    },
    setActive(state, value) {
        state.item.active = value
    },
    
    
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
