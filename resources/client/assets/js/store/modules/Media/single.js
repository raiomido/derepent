function initialState() {
    return {
        media: {},
        mediaLoading: false,
    }
}

const getters = {
    media: state => state.media,
    mediaLoading: state => state.mediaLoading,
};

const actions = {
    destroyMedia({ commit, state, dispatch}, ids) {

        commit('setLoading', true)

        let index = _.findIndex(state.media, ['id', ids.item]);

        if (index !== -1) {
            axios.delete('/api/v1/media/' + ids.item + '/i/' + ids.model)
                .then(response => {
                    commit('unsetMedia', index)
                }).catch(error => {
                let message = error.response.data.message || error.message
                let errors = error.response.data.errors;

                dispatch(
                    'Alert/setAlert',
                    {message: message, errors: errors, color: 'danger'},
                    {root: true})
            }).finally(() => {
                commit('setLoading', false)
            });
        }
    },
    setMedia({ commit }, media) {
        commit('setMedia', media)
    },
    setLoading({ commit }, loading) {
        commit('setLoading', loading)
    },
    resetState({ commit }) {
        commit('resetState')
    }
};

const mutations = {
    setMedia(state, media) {
        state.media = media
    },
    unsetMedia(state, index) {
        state.media.splice(index, 1);
    },
    setLoading(state, loading) {
        state.mediaLoading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
