function initialState() {
    return {
        item: {},
        loading : false
    }
}

const getters = {
    data: state => state.item,
    loading: state => state.loading,
}

const actions = {
    fetchData({commit, state, dispatch}, url) {
        commit('setLoading', true)

        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    let message = error.response.data.message || error.message

                    dispatch('Alert/setAlert', message, {root: true});

                    reject(error)
                })
                .finally(() => {
                    commit('setLoading', false)
                })
        })
    },

    resetState({commit}) {
        commit('resetState')
    }
}

const mutations = {
    setLoading(state, loading) {
        state.loading = loading
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
