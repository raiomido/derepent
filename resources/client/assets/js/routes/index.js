import Vue from 'vue'
import VueRouter from 'vue-router'

import ChangePassword from '../components/ChangePassword.vue'
import PermissionsIndex from '../components/cruds/Permissions/Index.vue'
import PermissionsCreate from '../components/cruds/Permissions/Create.vue'
import PermissionsShow from '../components/cruds/Permissions/Show.vue'
import PermissionsEdit from '../components/cruds/Permissions/Edit.vue'
import RolesIndex from '../components/cruds/Roles/Index.vue'
import RolesCreate from '../components/cruds/Roles/Create.vue'
import RolesShow from '../components/cruds/Roles/Show.vue'
import RolesEdit from '../components/cruds/Roles/Edit.vue'
import UsersIndex from '../components/cruds/Users/Index.vue'
import UsersCreate from '../components/cruds/Users/Create.vue'
import UsersShow from '../components/cruds/Users/Show.vue'
import UsersEdit from '../components/cruds/Users/Edit.vue'
import CategoriesIndex from '../components/cruds/Categories/Index.vue'
import CategoriesCreate from '../components/cruds/Categories/Create.vue'
import CategoriesShow from '../components/cruds/Categories/Show.vue'
import CategoriesEdit from '../components/cruds/Categories/Edit.vue'
import VideosIndex from '../components/cruds/Videos/Index.vue'
import VideosCreate from '../components/cruds/Videos/Create.vue'
import VideosShow from '../components/cruds/Videos/Show.vue'
import VideosEdit from '../components/cruds/Videos/Edit.vue'
import GalleriesIndex from '../components/cruds/Galleries/Index.vue'
import GalleriesCreate from '../components/cruds/Galleries/Create.vue'
import GalleriesShow from '../components/cruds/Galleries/Show.vue'
import GalleriesEdit from '../components/cruds/Galleries/Edit.vue'
import DocumentsIndex from '../components/cruds/Documents/Index.vue'
import DocumentsCreate from '../components/cruds/Documents/Create.vue'
import DocumentsShow from '../components/cruds/Documents/Show.vue'
import DocumentsEdit from '../components/cruds/Documents/Edit.vue'
import UserActionsIndex from '../components/cruds/UserActions/Index.vue'
import SlidersIndex from '../components/cruds/Sliders/Index.vue'
import SlidersCreate from '../components/cruds/Sliders/Create.vue'
import SlidersShow from '../components/cruds/Sliders/Show.vue'
import SlidersEdit from '../components/cruds/Sliders/Edit.vue'
import AnnouncementsIndex from '../components/cruds/Announcements/Index.vue'
import AnnouncementsCreate from '../components/cruds/Announcements/Create.vue'
import AnnouncementsShow from '../components/cruds/Announcements/Show.vue'
import AnnouncementsEdit from '../components/cruds/Announcements/Edit.vue'
import NuggetsIndex from '../components/cruds/Nuggets/Index.vue'
import NuggetsCreate from '../components/cruds/Nuggets/Create.vue'
import NuggetsShow from '../components/cruds/Nuggets/Show.vue'
import NuggetsEdit from '../components/cruds/Nuggets/Edit.vue'
import NuggetImages from '../components/cruds/Nuggets/Images.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/change-password', component: ChangePassword, name: 'auth.change_password' },
    { path: '/permissions', component: PermissionsIndex, name: 'permissions.index' },
    { path: '/permissions/create', component: PermissionsCreate, name: 'permissions.create' },
    { path: '/permissions/:id', component: PermissionsShow, name: 'permissions.show' },
    { path: '/permissions/:id/edit', component: PermissionsEdit, name: 'permissions.edit' },
    { path: '/roles', component: RolesIndex, name: 'roles.index' },
    { path: '/roles/create', component: RolesCreate, name: 'roles.create' },
    { path: '/roles/:id', component: RolesShow, name: 'roles.show' },
    { path: '/roles/:id/edit', component: RolesEdit, name: 'roles.edit' },
    { path: '/users', component: UsersIndex, name: 'users.index' },
    { path: '/users/create', component: UsersCreate, name: 'users.create' },
    { path: '/users/:id', component: UsersShow, name: 'users.show' },
    { path: '/users/:id/edit', component: UsersEdit, name: 'users.edit' },
    { path: '/categories', component: CategoriesIndex, name: 'categories.index' },
    { path: '/categories/create', component: CategoriesCreate, name: 'categories.create' },
    { path: '/categories/:id', component: CategoriesShow, name: 'categories.show' },
    { path: '/categories/:id/edit', component: CategoriesEdit, name: 'categories.edit' },
    { path: '/videos', component: VideosIndex, name: 'videos.index' },
    { path: '/videos/create', component: VideosCreate, name: 'videos.create' },
    { path: '/videos/:id', component: VideosShow, name: 'videos.show' },
    { path: '/videos/:id/edit', component: VideosEdit, name: 'videos.edit' },
    { path: '/galleries', component: GalleriesIndex, name: 'galleries.index' },
    { path: '/galleries/create', component: GalleriesCreate, name: 'galleries.create' },
    { path: '/galleries/:id', component: GalleriesShow, name: 'galleries.show' },
    { path: '/galleries/:id/edit', component: GalleriesEdit, name: 'galleries.edit' },
    { path: '/documents', component: DocumentsIndex, name: 'documents.index' },
    { path: '/documents/create', component: DocumentsCreate, name: 'documents.create' },
    { path: '/documents/:id', component: DocumentsShow, name: 'documents.show' },
    { path: '/documents/:id/edit', component: DocumentsEdit, name: 'documents.edit' },
    { path: '/user-actions', component: UserActionsIndex, name: 'user_actions.index' },
    { path: '/sliders', component: SlidersIndex, name: 'sliders.index' },
    { path: '/sliders/create', component: SlidersCreate, name: 'sliders.create' },
    { path: '/sliders/:id', component: SlidersShow, name: 'sliders.show' },
    { path: '/sliders/:id/edit', component: SlidersEdit, name: 'sliders.edit' },
    { path: '/announcements', component: AnnouncementsIndex, name: 'announcements.index' },
    { path: '/announcements/create', component: AnnouncementsCreate, name: 'announcements.create' },
    { path: '/announcements/:id', component: AnnouncementsShow, name: 'announcements.show' },
    { path: '/announcements/:id/edit', component: AnnouncementsEdit, name: 'announcements.edit' },
    { path: '/nuggets', component: NuggetsIndex, name: 'nuggets.index' },
    { path: '/nuggets/create', component: NuggetsCreate, name: 'nuggets.create' },
    { path: '/nuggets/:id', component: NuggetsShow, name: 'nuggets.show' },
    { path: '/nuggets/:id/edit', component: NuggetsEdit, name: 'nuggets.edit' },
    { path: '/nuggets/:id/images', component: NuggetImages, name: 'nuggets.images' },
]

export default new VueRouter({
    mode: 'history',
    base: '/admin',
    routes
})
