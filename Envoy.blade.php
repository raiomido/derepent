@servers(['prod' => 'root@167.71.155.211'])


@story('deploy', ['on' => 'prod'])
start
git
@if($composer)
    composer
@endif
permissions
rebuild-cache
finish
@endstory

@task('start')
cd /var/www/html/johndoe
php artisan down
echo 'Updating the production environment...'
@endtask

@task('finish')
cd /var/www/html/johndoe
php artisan up
echo 'All done!'
@endtask

@task('git')
cd /var/www/html/johndoe
git pull
@endtask

@task('composer')
cd /var/www/html/johndoe
if [ -d "vendor" ]; then
rm -rf vendor
fi
composer install --no-dev
@endtask

@task('permissions')
cd /var/www/html/johndoe
chown -R www-data storage/ bootstrap/cache/
chmod -R ug=rwx storage/ bootstrap/cache/
echo 'File permissions set successfully'
@endtask

@task('rebuild-cache')
cd /var/www/html/johndoe
php artisan view:clear
php artisan route:clear
php artisan cache:clear
php artisan config:clear
@endtask
