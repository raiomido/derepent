<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Announcement
 *
 * @package App
 * @property string $title
 * @property string $display_from
 * @property string $display_to
 * @property text $description
 * @property integer $order
 * @property string $active
*/
class Announcement extends Model
{
    use SoftDeletes;

    
    protected $fillable = ['title', 'display_from', 'display_to', 'description', 'order', 'active'];
    

    public static function boot()
    {
        parent::boot();

        Announcement::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'display_from' => 'date_format:' . config('app.date_format') . '|max:191|required',
            'display_to' => 'date_format:' . config('app.date_format') . '|max:191|required',
            'description' => 'max:65535|nullable',
            'order' => 'integer|max:4294967295|nullable',
            'active' => 'in:1,0|max:191|nullable',
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'display_from' => 'date_format:' . config('app.date_format') . '|max:191|required',
            'display_to' => 'date_format:' . config('app.date_format') . '|max:191|required',
            'description' => 'max:65535|nullable',
            'order' => 'integer|max:4294967295|nullable',
            'active' => 'in:1,0|max:191|nullable',
        ];
    }

    

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDisplayFromAttribute($input)
    {
        if ($input) {
            $this->attributes['display_from'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getDisplayFromAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDisplayToAttribute($input)
    {
        if ($input) {
            $this->attributes['display_to'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    public function getDisplayToAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }
    
    
}
