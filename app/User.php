<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Notifications\ResetPassword;
use Hash;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Laravel\Passport\HasApiTokens;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property string $avatar
*/
class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasMediaTrait {
        HasMediaTrait::addMedia as parentAddMedia;
    }
    use HasApiTokens;

    
    protected $fillable = ['name', 'email', 'password', 'remember_token'];
    protected $hidden = ['password', 'remember_token'];
    protected $appends = ['avatar', 'avatar_link', 'avatar_link_large'];
    protected $with = ['media'];

    public static function storeValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'email' => 'email|max:191|required|unique:users,email',
            'password' => 'required',
            'role' => 'array|required',
            'role.*' => 'integer|exists:roles,id|max:4294967295|required',
            'remember_token' => 'max:191|nullable',
            'avatar' => 'file|image|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'name' => 'max:191|required',
            'email' => 'email|max:191|required|unique:users,email,'.$request->route('user'),
            'password' => '',
            'role' => 'array|required',
            'role.*' => 'integer|exists:roles,id|max:4294967295|required',
            'remember_token' => 'max:191|nullable',
            'avatar' => 'nullable'
        ];
    }

    public static function boot()
    {
        parent::boot();

        User::observe(new \App\Observers\UserActionsObserver);
    }

    public function addMedia($file)
    {
        return $this->parentAddMedia($file)
            ->usingFileName($file->hashName());
    }
    
    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function getAvatarAttribute()
    {
        return $this->getFirstMedia('avatar');
    }

    /**
     * @return string
     */
    public function getAvatarLinkAttribute()
    {
        $file = $this->getFirstMedia('avatar');
        if (! $file) {
            return '<img src="' . url('/images/user-avatar-placeholder.png') . '" alt="user" class="rounded-circle" width="31">';
        }

        return '<img src="' . $file->getUrl() . '" alt="user" class="rounded-circle" width="31">';
    }

    /**
     * @return string
     */
    public function getAvatarLinkLargeAttribute()
    {
        $file = $this->getFirstMedia('avatar');
        if (! $file) {
            return '<img src="' . url('/images/user-avatar-placeholder.png') . '" alt="user" class="rounded-circle" width="60">';
        }

        return '<img src="' . $file->getUrl() . '" alt="user" class="rounded-circle" width="60">';
    }
    
    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }
    
    
    

    public function sendPasswordResetNotification($token)
    {
       $this->notify(new ResetPassword($token));
    }
}
