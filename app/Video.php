<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Video
 *
 * @package App
 * @property string $title
 * @property string $link
 * @property string $image
 * @property string $country
 * @property string $venue
 * @property string $date
 * @property text $description
 * @property integer $order
 * @property string $major_vision
 * @property string $fulfilled_prophecy
 * @property string $show_on_front_page
 * @property string $active
 */
class Video extends Model implements HasMedia
{
    use SoftDeletes;

    use HasMediaTrait {
        HasMediaTrait::addMedia as parentAddMedia;
    }

    protected $fillable = ['title', 'link', 'country', 'venue', 'date', 'description', 'order', 'major_vision', 'fulfilled_prophecy', 'show_on_front_page', 'active'];
    protected $appends = ['image', 'image_link', 'embed_link'];
    protected $with = ['media'];


    public static function boot()
    {
        parent::boot();

        Video::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'link' => 'max:191|required',
            'categories' => 'array|nullable',
            'categories.*' => 'integer|exists:categories,id|max:4294967295|nullable',
            'image' => 'file|image|nullable',
            'country' => 'max:191|nullable',
            'venue' => 'max:191|nullable',
            'date' => 'max:191|nullable',
            'description' => 'max:65535|nullable',
            'order' => 'integer|max:4294967295|nullable',
            'major_vision' => 'in:1,0|max:191|nullable',
            'fulfilled_prophecy' => 'in:1,0|max:191|nullable',
            'show_on_front_page' => 'in:1,0|max:191|nullable',
            'active' => 'in:1,0|max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'link' => 'max:191|required',
            'categories' => 'array|nullable',
            'categories.*' => 'integer|exists:categories,id|max:4294967295|nullable',
            'image' => 'nullable',
            'country' => 'max:191|nullable',
            'venue' => 'max:191|nullable',
            'date' => 'max:191|nullable',
            'description' => 'max:65535|nullable',
            'order' => 'integer|max:4294967295|nullable',
            'major_vision' => 'in:1,0|max:191|nullable',
            'fulfilled_prophecy' => 'in:1,0|max:191|nullable',
            'show_on_front_page' => 'in:1,0|max:191|nullable',
            'active' => 'in:1,0|max:191|nullable'
        ];
    }

    public function addMedia($file)
    {
        return $this->parentAddMedia($file)
            ->usingFileName($file->hashName());
    }

    public function getImageAttribute()
    {
        if (!$image = $this->getFirstMedia('image')) {
            return view('partials.default-image');
        }
        return $image;
    }

    /**
     * @return string
     */
    public function getImageLinkAttribute()
    {
        $file = $this->getFirstMedia('image');
        if (!$file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . '<img width="200" src="' . $file->getUrl() . '"' . ' alt="' . $file->file_name . '" ></a>';
    }

    /**
     * @return string
     */
    public function getEmbedLinkAttribute()
    {
        $url = $this->link;

        parse_str(parse_url($url, PHP_URL_QUERY), $url_vars_arr);

        if (isset($url_vars_arr['v'])) {
            $url = 'https://youtube.com/embed/' . $url_vars_arr['v'] . '?rel=0';
        }
        return $url;
    }

    public function scopeSearch(Builder $builder, $t) {
        return $builder->where('title', 'LIKE', "%$t%")->orWhere(function($q) use ($t) {
            $q->whereHas('categories', function (Builder $b)  use ($t){
                $b->where('title', 'LIKE', "%$t%");
            });
        });
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_video')->withTrashed();
    }


}
