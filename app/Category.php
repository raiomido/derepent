<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Category
 *
 * @package App
 * @property string $title
 * @property text $description
 * @property string $image
 * @property string $slug
 * @property string $active
 * @property integer $order
 */
class Category extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;


    protected $fillable = ['title', 'description', 'slug', 'active', 'order'];
    protected $appends = ['image', 'image_link'];
    protected $with = ['media'];


    public static function boot()
    {
        parent::boot();

        Category::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'description' => 'max:65535|required',
            'image' => 'file|image|nullable',
            'slug' => 'max:191|nullable',
            'active' => 'in:1,0|max:191|nullable',
            'order' => 'integer|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'description' => 'max:65535|required',
            'image' => 'nullable',
            'slug' => 'max:191|nullable',
            'active' => 'in:1,0|max:191|nullable',
            'order' => 'integer|max:4294967295|nullable'
        ];
    }

    public function getRouteKeyName()
    {
        if (request()->expectsJson()) {
            return 'id';
        }
        return 'slug';
    }

    public function getImageAttribute()
    {
        return $this->getFirstMedia('image');
    }

    public function setSlugAttribute($title)
    {
        if ($title) {
            $slug = Str::slug($title);
            do {
                $this->attributes['slug'] = $slug;
                $exists = Category::where('slug', $slug)->first();
                $slug = $slug . '-' . Str::random(3);
            } while ($exists);
        }
    }

    /**
     * @return string
     */
    public function getImageLinkAttribute()
    {
        $file = $this->getFirstMedia('image');
        if (!$file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . $file->file_name . '</a>';
    }

    public function scopeOtherCategories(Builder $builder)
    {
        $cats = $builder->whereNotIn('slug', [
            'prophecies-about-germany',
            'prophecies',
            'recent-prophecies',
            'general',
            'revival',
            'promo',
        ])->whereHas('videos', function ($q) {
            $q->where('active', "1");
        })->where('active', "1")->get();

        return $cats->each(function ($q) {
            $q->setRelation('videos', $q->videos()->where('active', 1)->orderBy('id', 'desc')->orderBy('order', 'asc')->take(6)->get());
        });
    }

    public function scopeProphecies(Builder $builder)
    {
        $cat = $builder->where('slug', 'prophecies')->where('active', "1")->first();

        return $cat ? $cat->videos()->whereNull('fulfilled_prophecy')->orWhere('fulfilled_prophecy', 0)
            ->whereNull('major_vision')->orWhere('major_vision', 0)
            ->orderBy('id', 'desc')->orderBy('order', 'asc')
            ->whereDoesntHave('categories', function ($q) {
                $q->where('id', 1);
            })->where('active', "1")->paginate(20) : $cat;
    }

    public function scopeOtherProphecies(Builder $builder, $prophecyType)
    {
        $cat = $builder->where('slug', 'prophecies')->where('active', "1")->first();
        return $cat ? $cat->videos()->where('active', "1")->where($prophecyType, 1)->orderBy('id', 'desc')->paginate(10) : $cat;
    }

    public function scopeSpecialCategoryVideos(Builder $builder, $category, $return_builder = false)
    {
        if ($cat = $builder->where('slug', $category)->where('active', "1")->first()) {
            $q = $cat->videos()->where('active', "1")->orderBy('id', 'desc');
            if ($return_builder)
                return $q;
            return $q->paginate(14);
        }
        return collect([]);
    }

    public function videos()
    {
        return $this->belongsToMany(Video::class, 'category_video');
    }
}
