<?php


namespace App\Raicasts\Mpesa\C2B;

use App\Account;
use App\Jobs\ProcessTransaction;
use App\MpesaC2B;
use App\Raicasts\Traits\Notifier;

class Validator
{
    use Notifier;

    /**
     * Response code to be returned to MpesaC2B
     * @var int
     */
    protected $resultCode = 0;

    /**
     * Response description to be returned to MpesaC2B
     * @var string
     */
    protected $resultDesc = 'FAILED';

    /**
     * Transaction id to be returned to mpesa
     * @var int
     */
    protected $thirdPartyTransID = 0;

    protected $payload;

    protected $token;

    protected $account;

    private $error = false;

    private $errorMsg = '';

    /**
     * @param $payload
     * @return $this
     */
    public function setPayload($payload)
    {

        $this->payload = collect($payload);

        return $this;
    }

    /**
     * @param $token
     * @return $this
     */
    public function setToken($token)
    {

        $this->token = $token;

        return $this;
    }

    /**
     * @param $reference
     * @return $this
     */
    public function setAccount($reference)
    {


        if (!$account = Account::where('number', $reference)->first()) {
            $this->error = true;
            $this->resultDesc = $this->errorMsg = 'Invalid account number';
        }

        $this->account = $account;

        return $this;
    }

    public function validate()
    {

        $validationToken = config('sys.service.mpesa.validation_key');
        $token = $this->token;

        if ($token != $validationToken) {
            $this->error = true;
            $this->errorMsg = "MpesaC2B transaction rejected at esg validation. Validation token mismatch";
        }

        if (!$this->payload) {
            $this->error = true;
            $this->errorMsg = "MpesaC2B transaction rejected at esg validation. Empty payload";
        }

        return $this->respond();
    }

    public function confirm()
    {

        $confirmationToken = config('sys.service.mpesa.confirmation_key');
        $token = $this->token;

        if ($token != $confirmationToken) {
            $this->error = true;
            $this->errorMsg = 'MpesaC2B transaction rejected at confirmation. confirmation token mismatch';
        }

        if (!$this->payload) {
            $this->error = true;
            $this->errorMsg = 'MpesaC2B transaction rejected at confirmation. Empty payload';
        }

        # Log the mpesa transaction to db if there were no errors
        if(!$this->error) {
            $mpesaC2B = MpesaC2B::create([
                'type' => $this->payload->get('TransactionType'),
                'mpesa_transaction_id' => $this->payload->get('TransID'),
                'time' => $this->payload->get('TransTime'),
                'amount' => to_currency('KES', 'USD', $this->payload->get('TransAmount')),
                'short_code' => $this->payload->get('BusinessShortCode'),
                'bill_ref_number' => $this->payload->get('BillRefNumber'),
                'invoice_number' => $this->payload->get('InvoiceNumber'),
                'msisdn' => $this->payload->get('MSISDN'),
                'first_name' => $this->payload->get('FirstName'),
                'middle_name' => $this->payload->get('MiddleName'),
                'last_name' => $this->payload->get('LastName'),
                'org_account_balance' => $this->payload->get('OrgAccountBalance'),
            ]);

            //Process the transaction
            ProcessTransaction::dispatch($mpesaC2B->bill_ref_number, $mpesaC2B, $mpesaC2B->amount);
        }

        return $this->respond();
    }

    private function respond()
    {

        if (!$this->error) {
            $this->resultCode = 1;
            $this->resultDesc = 'Success';
        } else {
            $this->toSlack($this->errorMsg);
        }

        return [
            'ResultCode' => $this->resultCode,
            'ResultDesc' => $this->resultDesc,
            'ThirdPartyTransID' => $this->thirdPartyTransID,
        ];
    }


}