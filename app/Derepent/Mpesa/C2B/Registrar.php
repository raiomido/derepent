<?php


namespace App\Raicasts\Mpesa\C2B;


use GuzzleHttp\Exception\RequestException;
use App\Raicasts\Mpesa\Core\Core;
use App\Raicasts\Mpesa\Core\Traits\MakesRequests;

class Registrar
{
    use MakesRequests;
    /**
     * MpesaC2B paybill/buy goods number
     * @var string
     */
    protected $shortCode;

    /**
     * Action to be taken by MpesaC2B after validation
     * @var string Completed|Cancelled
     */
    protected $responseStatus = 'Completed';

    /**
     * Validation callback url
     * @var string
     */
    protected $validationUrl;

    /**
     * Confirmation callback url
     * @var string
     */
    protected $confirmationUrl;


    /**
     * Valid timeout statuses
     */
    const VALID_RESPONSE_STATUSES = ['Completed', 'Cancelled'];

    /**
     * Add shortCode to be registered
     * @param $shortCode
     * @return $this
     */
    public function register($shortCode) {
        $this->shortCode = $shortCode;
        return $this;
    }

    /**
     * Set the validation url to be submitted
     * @param string $validationUrl
     * @return $this
     */
    public function validateOn(string $validationUrl) {
        $this->validationUrl = $validationUrl;
        return $this;
    }

    /**
     * Set the confirmation url to be submitted
     * @param string $confirmationUrl
     * @return $this
     */
    public function confirmOn(string $confirmationUrl) {
        $this->confirmationUrl = $confirmationUrl;
        return $this;
    }

    /**
     * Set action to be taken after validation | confirmation
     * @param string $status
     * @return $this
     */
    public function respond(string $status) {
        if (!in_array($status, self::VALID_RESPONSE_STATUSES)) {
            throw new \InvalidArgumentException('Invalid response status argument');
        }
        $this->responseStatus = $status;
        return $this;
    }

    /**
     * @param string|null $shortCode
     * @param string|null $validationUrl
     * @param string|null $confirmationUrl
     * @param string|null $responseStatus
     * @return mixed|\Psr\Http\Message\ResponseInterface|null
     * @throws \ErrorException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function submit(string $shortCode = null, string $validationUrl = null, string $confirmationUrl = null, string $responseStatus = null) {

        if($responseStatus) {
            $this->respond($responseStatus);
        }

        $body = [
            'ShortCode'       => $shortCode ?: $this->shortCode,
            'ResponseType'    => $responseStatus ?: $this->responseStatus,
            'ConfirmationURL' => $confirmationUrl ?: $this->confirmationUrl,
            'ValidationURL'   => $validationUrl ?: $this->validationUrl,
        ];

        try {

            $response = $this->makeRequest($body, Core::getEndpoint('c2b', 'register'));

        } catch (RequestException $e) {

            return $e->getResponse();
        }

        return $response;
    }

}