<?php


namespace Mochange\Esg\Core\Mpesa\C2B;

use GuzzleHttp\Exception\RequestException;
use App\Raicasts\Mpesa\Core\Core;
use App\Raicasts\Mpesa\Core\Traits\MakesRequests;

class Simulator
{
    use MakesRequests;
    /**
     * MpesaC2B paybill number
     * @var string
     */
    protected $shortCode;

    /**
     * Phone number
     * @var string
     */
    protected $msisdn;

    /**
     *
     * @var string
     */
    protected $commandId = 'CustomerPayBillOnline';

    /**
     * Amount to be debited
     * @var int
     */
    protected $amount;

    /**
     * User account number
     * @var string
     */
    protected $reference;

    /**
     * List of commands accepted by MpesaC2B
     */
    const VALID_COMMANDS = ['CustomerPayBillOnline'];

    /**
     * Set amount to be debited
     * @param $amount
     * @return $this
     */
    public function charge($amount)
    {
        if(!is_numeric($amount)) {
            throw new \InvalidArgumentException('Invalid argument supplied for amount');
        }
        $this->amount = $amount;

        return $this;
    }

    /**
     * Set mpesa number to be debited
     * @param $phone
     * @return $this
     */
    public function from($phone)
    {
        if(!starts_with($phone, '254')) {
            throw new \InvalidArgumentException('Invalid argument supplied for msisdn. Must start with 254');
        }

        $this->msisdn = $phone;

        return $this;
    }

    /**
     * Set account to be credited
     * @param $reference
     * @return $this
     */
    public function usingReference($reference)
    {

        $this->reference = $reference;

        return $this;
    }

    /**
     * Set the unique command for this transaction type.
     * @param $command
     * @return $this
     */
    public function commandId($command)
    {
        if (!in_array($command, self::VALID_COMMANDS)) {
            throw new \InvalidArgumentException('Invalid argument supplied for commandId');
        }
        $this->commandId = $command;
        return $this;
    }

    /**
     * @param null $amount
     * @param null $number
     * @param null $reference
     * @param null $command
     * @return mixed|\Psr\Http\Message\ResponseInterface|null
     * @throws \ErrorException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function simulate($amount = null, $number = null, $reference = null, $command = null) {

        if(!config('sys.service.mpesa.env') == 'sandbox') {
            throw new \ErrorException('Cannot simulate a transaction in a live environment');
        }

        $body = [
            'CommandID'     => $command ?: $this->commandId,
            'Amount'        => $amount ?: $this->amount,
            'Msisdn'        => $number ?: $this->msisdn,
            'ShortCode'     => Core::getShortCode(),
            'BillRefNumber' => $reference ?: $this->reference,
        ];

        try {
            $response = $this->makeRequest($body, Core::getEndpoint('c2b', 'simulate'));
            logger($body);
        } catch (RequestException $e) {

            return $e->getResponse();
        }

        return $response;
    }
}