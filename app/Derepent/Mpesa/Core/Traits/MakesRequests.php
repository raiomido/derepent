<?php


namespace App\Raicasts\Mpesa\Core\Traits;
use App\Raicasts\Mpesa\Core\Core;
use GuzzleHttp\Client;

trait MakesRequests
{
    /**
     * @param array $body
     * @param string $endpoint
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function makeRequest(array $body, string $endpoint)
    {
        $client = new Client();
        return $client->request('POST', $endpoint, [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->getAccessToken(),
                'Content-Type'  => 'application/json',
            ],
            'json' => $body,
        ]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */

    private function getAccessToken()
    {
        $token = cache()->remember('mpesa_auth_token', 59, function () {
            $client = new Client();
            $req =  $client->request('GET', Core::getEndpoint('c2b', 'auth_token'), [
                'headers' => [
                    'Authorization' => 'Authorization: Basic ' . Core::getPassword(),
                    'Content-Type'  => 'application/json',
                ],
            ]);

//            $response = Curl::to()
//                ->withHeader('Authorization: Basic ' . Core::getPassword())
//                ->withOption('HEADER', false)
//                ->withOption('RETURNTRANSFER', 1)
//                ->withOption('SSL_VERIFYPEER', false)
//                ->returnResponseObject()
//                ->asJson()
//                ->get();

            return $req->getBody()->access_token;
        });

        return $token;

    }

}