<?php


namespace App\Raicasts\Mpesa\Core;

class Core
{
    const VALID_MPESA_ENVIRONMENTS = ['live', 'sandbox'];
    const VALID_MPESA_APP_ENVIRONMENTS = ['production', 'local', 'development'];

    /**
     * Get the specified endpoint from the config file
     * @param $mpesaApi
     * @param $endpoint
     * @return mixed
     * @throws \ErrorException
     */
    public static function getEndpoint($mpesaApi, $endpoint) {
        $env = config('sys.service.mpesa.env');

        if(!in_array($env, self::VALID_MPESA_ENVIRONMENTS)) {
            throw new \ErrorException("Could not determine Mpesa environment");
        }

        if(!$mpesaApiEndpoints = config("sys.service.mpesa.{$env}.{$mpesaApi}.url")) {
            throw new \InvalidArgumentException("Invalid argument supplied for Mpesa api");
        }

        if(!isset($mpesaApiEndpoints[$endpoint])) {
            throw new \ErrorException("Supplied Mpesa endpoint is not set");
        }

        return $mpesaApiEndpoints[$endpoint];

    }

    /**
     * Get the pmt endpoint to send mpesa payload after confirmation
     * @param $mpesaApi
     * @return mixed
     * @throws \ErrorException
     */
    public static function getPMTEndpoint($mpesaApi) {
        $appEnv = config('sys.service.mpesa.app_env');

        if(!in_array($appEnv, self::VALID_MPESA_APP_ENVIRONMENTS)) {
            throw new \UnexpectedValueException("Unexpected value set for Mpesa app environment. Expecting 'production', or 'local' or 'dev'");
        }

        if(!$pmtApi = config("sys.service.pmt_api.mpesa.{$mpesaApi}")) {
            throw new \ErrorException("Mpesa api supplied not set");
        }

        return $pmtApi[$appEnv];
    }

    /**
     * @return string
     * @throws \ErrorException
     */
    public static function getPassword() {
        $env = config('sys.service.mpesa.env');

        if(!in_array($env, self::VALID_MPESA_ENVIRONMENTS)) {
            throw new \UnexpectedValueException("Unexpected value set for Mpesa environment. Expecting 'live', or 'sandbox'");
        }
        $consumerKey = config("sys.service.mpesa.{$env}.consumer_key");
        $consumerSecret = config("sys.service.mpesa.{$env}.consumer_secret");

        if(!$consumerKey || !$consumerKey) {
            throw new \ErrorException("Could not load mpesa consumer key or secret");
        }

        $credentials = $consumerKey . ':' . $consumerSecret;

        return base64_encode($credentials);
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     * @throws \ErrorException
     */
    public static function getConfirmationKey() {

        if(!$confirmationKey = config("sys.service.mpesa.confirmation_key")) {
            throw new \ErrorException("Could not load mpesa confirmation key");
        }

        return $confirmationKey;
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     * @throws \ErrorException
     */
    public static function getValidationKey() {

        if(!$validationKey = config("sys.service.mpesa.validation_key")) {
            throw new \ErrorException("Could not load mpesa validation key");
        }

        return $validationKey;
    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     * @throws \ErrorException
     */
    public static function getShortCode() {
        $env = config('sys.service.mpesa.env');

        if(!in_array($env, self::VALID_MPESA_ENVIRONMENTS)) {
            throw new \UnexpectedValueException("Unexpected value set for Mpesa environment. Expecting 'live', or 'sandbox'");
        }

        if(!$shortCode = config("sys.service.mpesa.{$env}.short_code")) {
            throw new \ErrorException("Could not load mpesa short code");
        }

        return $shortCode;
    }


}