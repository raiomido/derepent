<?php
/**
 * Created by PhpStorm.
 * User: raiomido
 * Date: 3/7/19
 * Time: 1:01 PM
 */

namespace App\Raicasts\View;

use App\Category;
use App\Raicasts\Helpers\Utility;
use Illuminate\View\View;


class AppComposer
{

    private $naturalPageTitle;
    private $naturalPageDescription;
    private $naturalPageKeywords;
    private $controller;
    private $action;
    private $live_video = null;

    /**
     * Create a new app composer.
     * @return void
     */
    public function __construct()
    {
        $this->initPageParams();
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'naturalPageTitle' => $this->naturalPageTitle,
            'naturalPageDescription' => $this->naturalPageDescription,
            'naturalPageKeywords' => $this->naturalPageKeywords,
            'controller' => $this->controller,
            'action' => $this->action,
            'naturalOgImage' => url('images/og-placeholder.jpg'),
            'user' => ($user = auth()->user()),
            'website_info' => $this->websiteInfo(),
            'live_video' => $this->live_video,
        ]);
    }

    public function initPageParams()
    {
        $route_arr = request()->route() ? explode('.', request()->route()->getName()) : '';
        $key1 = isset($route_arr[1]) ? $route_arr[1] : '';
        $key0 = isset($route_arr[0]) ? $route_arr[0] : '';
        $this->controller = $key0;
        $this->action = $key1;
        $title = $this->action ? preg_replace('/[^a-z\d ]/i', " ", $this->action) : preg_replace('/[^a-z\d ]/i', " ",
            $this->controller);
        $this->naturalPageTitle = ucwords($title);
        $this->naturalPageKeywords = __('Ministry of Repentance and Holiness Germany');
        $this->naturalPageDescription = __('site.page-description.home');

        if ($cat = Category::where('slug', 'live')->first())
            $this->live_video = $cat->videos()->where('show_on_front_page', '1')->orderBy('id', 'DESC')->first();
    }

    private function websiteInfo()
    {
        return json_decode(json_encode(config('sys.settings.website_info')));
    }
}
