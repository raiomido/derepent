<?php
/**
 * Created by PhpStorm.
 * User: raiomido
 * Date: 6/19/19
 * Time: 7:58 PM
 */

namespace App\Raicasts\Helpers;


use App\Transaction;
use App\User;
use Illuminate\Support\Facades\Storage;
use Barryvdh\DomPDF\Facade as PDF;
use Intervention\Image\Facades\Image;

class Utility
{

    public static function objectFromArray(array $arr)
    {

        return json_decode(json_encode($arr));
    }

    public static function generateUserAccountNumber()
    {

        return ReferenceCodes::generate([
            "length" => 8,
            "prefix" => "RC",
            "suffix" => "",
            "numbers" => true,
            "letters" => false,
        ]);
    }

    public static function generateSlug($concat)
    {
        $options = [
            "length" => 4,
            "numbers" => true,
            "letters" => true,
            "mixed_case" => true,
        ];
        return ReferenceCodes::generate($options) .$concat . ReferenceCodes::generate($options);
    }

    public static function companyDetails()
    {

        $companyDetails = ($details = config('sys.company')) ? $details : [];

        return self::objectFromArray($companyDetails);

    }

    public static function deleteImage(string $name, string $disk)
    {

        return Storage::disk($disk)->delete($name);

    }

    public static function saveImage($file, $name, string $disk)
    {

        $ext = $file->getClientOriginalExtension();
        $fileName = time() . "-" . $name . ".$ext";
        $image = Image::make($file->getRealPath());
        $image->stream();
        $thumb = $image->resize(500, 500, function ($constraint) {

            $constraint->aspectRatio();
        });
        Storage::disk($disk)->put("/$fileName", $thumb);

        return $fileName;

    }


    public static function strTitle($string)
    {

        return ucwords(strtolower(str_replace('_', ' ', $string)));
    }

}