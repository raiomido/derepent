<?php
// namespace App\Http;
use Illuminate\Support\Facades\Storage;

if (!function_exists('is_active')) {
    function is_active($url)
    {
        if (is_array($url)) {
            $active = 0;
            foreach ($url as $link) {
                if (request()->is($link))
                    $active++;
            }
        }
        return request()->is($url) ? 'active' : '';
    }
}

if (!function_exists('str_slug')) {
    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param string $title
     * @param string $separator
     * @return string
     */
    function str_slug($title, $separator = '-')
    {
        return \App\Raicasts\Lib\Str::slug($title, $separator);
    }
}

if (!function_exists('is_cli')) {
    function is_cli()
    {
        if (defined('STDIN')) {
            return true;
        }

        if (empty($_SERVER['REMOTE_ADDR']) and !isset($_SERVER['HTTP_USER_AGENT']) and count($_SERVER['argv']) > 0) {
            return true;
        }

        return false;
    }
}

if (!function_exists('is_cgi_console')) {
    function is_cgi_console()
    {
        return is_cli() && stristr(PHP_SAPI, 'cgi') and getenv('TERM');
    }
}

