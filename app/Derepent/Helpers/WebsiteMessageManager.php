<?php


namespace App\Easyanp\Helpers;


use Illuminate\Support\HtmlString;

class WebsiteMessageManager
{
	protected $firstname;
	protected $lastname;
	protected $phone;
	protected $email;
	protected $enquiry;
	protected $country;
	protected $city;
	protected $message;
	protected $userMessage;


	public function firstName(string $firstname)
	{
		$this->firstname = $firstname;

		return $this;
	}

	public function lastName(string $lastname)
	{
		$this->lastname = $lastname;

		return $this;
	}

	public function email(string $email)
	{
		$this->email = $email;

		return $this;
	}

	public function phone(string $phone)
	{
		$this->phone = $phone;

		return $this;
	}

	public function enquiry(string $enquiry)
	{
		$this->enquiry = $enquiry;

		return $this;
	}

	public function country(string $country)
	{
		$this->country = $country;

		return $this;
	}

	public function city(string $city)
	{
		$this->city = $city;

		return $this;
	}

	public function message(string $message)
	{
		$this->message = $message;

		return $this;
	}

	/**
	 * @return $this
	 */
	public function manage()
	{
		$message = '<b>Greetings</b><br><br>';
		$message .= "Someone by the name  {$this->firstname} {$this->lastname} has sent you an enquiry from the wild vision website.";
		$message .= "<br><br><b>Email: </b> {$this->email}";
		$message .= "<br><b>Phone: </b> {$this->phone}";
		$message .= "<br><b>Country: </b> {$this->country}";
		$message .= "<br><b>City: </b> {$this->city}";
		$message .= "<br><b>Enquiring About: </b> {$this->enquiry}";
		$message .= "<br><br><b>Message: </b><br>";
		$message .= htmlspecialchars($this->message);

		$this->userMessage = new HtmlString($message);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return "{$this->firstname} {$this->lastname}";
	}
	/**
	 * @return mixed
	 */
	public function getUserMessage()
	{
		return $this->userMessage;
	}


}