<?php


namespace App\Raicasts\Traits;


use App\Notifications\DevSlackNotification;
use App\Notifications\TransactionMade;
use App\Transaction;
use Illuminate\Support\Facades\Notification;

trait Notifier
{
    public function toSlack(string $message) {
        #Send a slack message | For testing. Send proper notifications in production
        Notification::route('slack', config('sys.service.general.slack.webhook'))
            ->notify(new DevSlackNotification($message));
    }

    public function notifyAccountHolderOfTransaction(Transaction $transaction, string $entryType) {
        $originalAmount = to_currency('USD', $transaction->currency_code, $transaction->amount);

        $amountInfo = $transaction->currency_code . ' ' . $originalAmount;

        $gateway = format_gateway($transaction->loggable_type);
        $userMsg = $entryType == 'CREDIT'
            ? "Your account was successfully credited with {$amountInfo} from " . $gateway
            : "{$amountInfo} from your account was successfully paid to {$gateway} for purchase of a subscription plan";

        $transaction->transactable->accountable->notify(new TransactionMade('Transaction',
            "Ref {$transaction->reference}. $userMsg  on {$transaction->created_at->formatLocalized('%d %B  %Y at %H:%M:%S')}"));
    }
}