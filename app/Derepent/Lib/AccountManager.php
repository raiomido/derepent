<?php


namespace App\Raicasts\Lib;

use App\Account;
use App\Raicasts\Helpers\ReferenceCodes;
use App\Transaction;

/**
 * Balances accounts after deposit or transfer and transactions recorded
 * Class AccountManager
 * @package App\Derepent\Lib
 */
class AccountManager
{
    public function makeEntry(TransactionManager $manager)
    {

        $this->makeEntryRecord(
            $manager->getTransaction(),
            $manager->getAccount(),
            $manager->getAccountEntryType());
        return $this;
    }

    /**
     * Record an entry into an account
     * @param Transaction $transaction
     * @param Account $account
     * @param string $entryType
     */
    private function makeEntryRecord(
        Transaction $transaction,
        Account $account,
        string $entryType
    ) {

        $amount = $transaction->amount + $transaction->cost;
        $balanceBeforeEntry = $account->balance;
        $balanceAfterEntry = $entryType == "CREDIT" ? $balanceBeforeEntry + $amount : $balanceBeforeEntry - $amount;

        $account->update(['balance' => $balanceAfterEntry,]);

        # Record wallet entry
        $account->entries()->create([
            'reference' => $transaction->reference,
            'balance_before' => $balanceBeforeEntry,
            'balance_after' => $balanceAfterEntry,
            'type' => $entryType,
            'amount' => $amount,
        ]);

    }

}