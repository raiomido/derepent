<?php


namespace App\Raicasts\Lib;


use App\Currency;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Account;
use App\Transaction;
use App\Raicasts\Helpers\ReferenceCodes;

/**
 * Records transactions after deposit/transfer
 * Class TransactionManager
 * @package App\Derepent\Mpesa\C2B
 */
class TransactionManager
{
    /**
     * @var Account $account
     */
    protected $account;

    /**
     * @var string $number
     */
    protected $number;

    /**
     * @var $loggable
     */
    protected $loggable;

    /**
     * @var Transaction $transaction
     */
    protected $transaction;

    /**
     * Amount paid in
     * @var string $amount
     */
    protected $amount;

    /**
     * Currency used to pay in
     * @var object $currency
     */
    protected $currency;

    /**
     * DEBIT or CREDIT
     * @var string $accountEntryType
     */
    protected $accountEntryType;

    public function addCreditTransaction()
    {

        $this->accountEntryType = 'CREDIT';

        return $this;
    }

    public function addDebitTransaction()
    {

        $this->accountEntryType = 'DEBIT';

        return $this;
    }

    public function worth(int $amount)
    {

        $this->amount = $amount;

        return $this;
    }

    /**
     * Load the transaction subject account from account number
     * @param string $number
     * @return $this
     */
    public function onAccount(string $number)
    {

        if (!$this->account = Account::where('number', $number)->first()) {

            throw new ModelNotFoundException('No account was found for the specified number');
        }

        $this->number = $number;

        return $this;
    }

    /**
     * Set the transaction account in case already loaded
     * @param Account $account
     * @return $this
     */
    public function setAccount(Account $account)
    {

        $this->account = $account;
        $this->number = $account->number;

        return $this;
    }

    /**
     * Set log currency
     * @param string $code
     * @return $this
     */
    public function setCurrency(string $code)
    {
        if (!$this->currency = Currency::where('code', $code)->first()) {

            throw new ModelNotFoundException('No currency was found for the specified code');
        }

        return $this;
    }

    /**
     * @param string $entryType
     * @return $this
     */
    public function setEntryType(string $entryType)
    {
        $this->accountEntryType = $entryType;

        return $this;
    }

    /**
     * @param object $loggable
     * @return $this
     */
    public function fromGateway($loggable)
    {
        $this->loggable = $loggable;

        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {

        return $this->account;
    }

    /**
     * @return int
     */
    public function getAmount()
    {

        return $this->amount;
    }

    /**
     * @return object
     */
    public function getCurrency()
    {

        return $this->currency;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {

        return $this->currency->code ?? 'USD';
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {

        return $this->number;
    }


    /**
     * @return string
     */
    public function getAccountEntryType()
    {

        return $this->accountEntryType;
    }

    /**
     * @return Transaction
     */
    public function getTransaction()
    {

        return $this->transaction;
    }

    public function save()
    {

        $this->transaction = $trx = $this->account->transactions()->create([
            'reference' => rand(),
            'amount' => $this->amount,
            'currency_code' => $this->currency->code,//Code of the Currency used to pay in
            'status' => 'PENDING',
        ]);


        # Update transaction reference
        $reference = ReferenceCodes::generate([
            "length" => 4,
            "prefix" => "RCT",
            "suffix" => $trx->id,
            "numbers" => true,
            "letters" => true,
        ]);

        $trx->update(['reference' => strtoupper($reference)]);

        # Change transaction status to processing
        $this->transaction->update([
            'status' => "PROCESSING",
            'loggable_id' => $this->loggable->id,
            'loggable_type' => get_class($this->loggable),
        ]);

        return $this;
    }

}