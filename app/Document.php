<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Document
 *
 * @package App
 * @property string $title
 * @property string $link
 * @property string $file
 * @property string $image
 * @property text $description
 * @property string $show_on_front_page
 * @property integer $order
 * @property string $active
*/
class Document extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait {
        HasMediaTrait::addMedia as parentAddMedia;
    }

    
    protected $fillable = ['title', 'link', 'description', 'show_on_front_page', 'order', 'active'];
    protected $appends = ['file', 'file_link', 'image', 'image_link'];
    protected $with = ['media'];
    

    public static function boot()
    {
        parent::boot();

        Document::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'link' => 'max:191|nullable',
            'file' => 'file|nullable',
            'image' => 'file|image|nullable',
            'description' => 'max:65535|nullable',
            'show_on_front_page' => 'in:1|max:191|nullable',
            'order' => 'integer|max:4294967295|nullable',
            'active' => 'in:1,0|max:191|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|required',
            'link' => 'max:191|nullable',
            'file' => 'nullable',
            'image' => 'nullable',
            'description' => 'max:65535|nullable',
            'show_on_front_page' => 'in:1|max:191|nullable',
            'order' => 'integer|max:4294967295|nullable',
            'active' => 'in:1,0|max:191|nullable'
        ];
    }

    public function addMedia($file)
    {
        return $this->parentAddMedia($file)
            ->usingFileName($file->hashName());
    }

    public function getFileAttribute()
    {
        return $this->getFirstMedia('file');
    }

    /**
     * @return string
     */
    public function getFileLinkAttribute()
    {
        $file = $this->getFirstMedia('file');
        if (! $file) {
            return null;
        }

        return $file->getUrl();
    }

    public function getImageAttribute()
    {
        return $this->getFirstMedia('image');
    }

    /**
     * @return string
     */
    public function getImageLinkAttribute()
    {
        $file = $this->getFirstMedia('image');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . '<img width="200" src="'.$file->getUrl().'"' . ' alt="'.$file->file_name.'" ></a>';
    }
    
    
}
