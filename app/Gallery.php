<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Gallery
 *
 * @package App
 * @property string $title
 * @property text $description
 * @property string $video_link
 * @property integer $order
 * @property string $show_on_front_page
 * @property string $active
 */
class Gallery extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait {
        HasMediaTrait::addMedia as parentAddMedia;
    }


    protected $fillable = ['title', 'description', 'video_link', 'order', 'show_on_front_page', 'active'];
    protected $appends = ['images', 'images_link', 'uploaded_images'];
    protected $with = ['media'];


    public static function boot()
    {
        parent::boot();

        Gallery::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|nullable',
            'description' => 'max:65535|nullable',
            'images' => 'required',
            'images.*' => 'file|image|required',
            'video_link' => 'max:191|nullable',
            'order' => 'integer|max:4294967295|nullable',
            'show_on_front_page' => 'in:1,0|max:191|nullable',
            'active' => 'in:1,0|max:191|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|nullable',
            'description' => 'max:65535|nullable',
            'images' => 'sometimes',
//            'images.*' => 'file|image|nullable',
            'video_link' => 'max:191|nullable',
            'order' => 'integer|max:4294967295|nullable',
            'show_on_front_page' => 'in:1,0|max:191|nullable',
            'active' => 'in:1,0|max:191|required'
        ];
    }

    public function addMedia($file)
    {
        return $this->parentAddMedia($file)
            ->usingFileName($file->hashName());
    }

    public function getImagesAttribute()
    {
        return $this->getMedia('images')->keyBy('id');
    }

    public function getUploadedImagesAttribute()
    {
        return $this->getMedia('images')->keyBy('id');
    }

    /**
     * @return string
     */
    public function getImagesLinkAttribute()
    {
        $images = $this->getMedia('images');
        if (!count($images)) {
            return null;
        }
        $html = [];
        foreach ($images as $file) {
            $html[] = '<a href="' . $file->getUrl() . '" target="_blank">' . '<img width="200" src="'.$file->getUrl().'"' . ' alt="'.$file->file_name.'" ></a>';
        }

        return implode('<br/>', $html);
    }

    public function scopeFrontPageGalleries(Builder $builder) {
        return $builder->where('show_on_front_page', 1)->where('active', 1)->orderBy('id', 'desc');
    }

    public function scopeOtherGalleries(Builder $builder) {
        return $builder->where('active', 1)->orderBy('id', 'desc')->orderBy('order', 'asc');
    }

}
