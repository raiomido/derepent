<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Slider
 *
 * @package App
 * @property string $title
 * @property text $description
 * @property string $image
 * @property string $video_link
 * @property string $active
 * @property integer $order
*/
class Slider extends Model implements HasMedia
{
    use SoftDeletes;

    use HasMediaTrait {
        HasMediaTrait::addMedia as parentAddMedia;
    }


    protected $fillable = ['title', 'description', 'video_link', 'active', 'order'];
    protected $appends = ['image', 'image_link'];
    protected $with = ['media'];


    public static function boot()
    {
        parent::boot();

        Slider::observe(new \App\Observers\UserActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|nullable',
            'description' => 'max:65535|nullable',
            'image' => 'file|image|required',
            'video_link' => 'max:191|nullable',
            'active' => 'in:1,0|max:191|nullable',
            'order' => 'integer|max:4294967295|nullable'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|nullable',
            'description' => 'max:65535|nullable',
            'image' => 'nullable',
            'video_link' => 'max:191|nullable',
            'active' => 'in:1,0|max:191|nullable',
            'order' => 'integer|max:4294967295|nullable'
        ];
    }

    public function addMedia($file)
    {
        return $this->parentAddMedia($file)
            ->usingFileName($file->hashName());
    }

    public function getImageAttribute()
    {
        return $this->getFirstMedia('image');
    }

    /**
     * @return string
     */
    public function getImageLinkAttribute()
    {
        $file = $this->getFirstMedia('image');
        if (! $file) {
            return null;
        }

        return '<a href="' . $file->getUrl() . '" target="_blank">' . '<img width="200" src="'.$file->getUrl().'"' . ' alt="'.$file->file_name.'" ></a>';
    }

    /**
     * @return string
     */
    public function getImageUrlAttribute()
    {
        $file = $this->getFirstMedia('image');
        if (! $file) {
            return null;
        }

        return $file->getUrl();
    }


}
