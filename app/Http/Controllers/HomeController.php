<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Category;
use App\Gallery;
use App\Notifications\General;
use App\Document;
use App\Nugget;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{

    /**
     * Show the application home page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $today = date("Y-m-d");

        return view('site.index', [
            'sliders' => Slider::orderBy('id', 'desc')->orderBy('order', 'asc')->where('active', "1")->take(20)->get(),
            'announcements' => Announcement::where('active', "1")
                ->where([
                    ['active', '=', 1],
                    ['display_from', '<=', $today],
                    ['display_to', '>=', $today],
                ])
                ->get(),
            'prophecies' => Category::prophecies(),
            'fulfilled_prophecies' => Category::otherProphecies('fulfilled_prophecy'),
            'recent_prophecies' => Category::specialCategoryVideos('recent-prophecies'),
            'major_visions' => Category::otherProphecies('major_vision'),
            'prophecies_about_germany' => Category::specialCategoryVideos('prophecies-about-germany'),
            'general_videos' => Category::specialCategoryVideos('general'),
            'revival' => Category::specialCategoryVideos('revival'),
            'documents' => Document::where('active', "1")->orderBy('id', 'desc')->get(),
            'other_categories' => Category::otherCategories(),
            'galleries' => Gallery::frontPageGalleries()->simplePaginate(6),
            'nuggets' => Nugget::latestNuggets(),
            'promo' => Category::specialCategoryVideos('promo')->first(),
            'pageTitle' => __('Home'),
            'pageDescription' => __('site.page-description.home'),
        ]);
    }

    public function prophecies()
    {
        return view('site.videos', [
            'videos' => Category::prophecies(),
            'pageTitle' => __('Prophecies'),
            'pageDescription' => __('site.page-description.prophecies'),
        ]);
    }

    public function fulfilledProphecies()
    {

        return view('site.videos', [
            'videos' => Category::otherProphecies('fulfilled_prophecy'),
            'pageTitle' => __('Fulfilled Prophecies'),
            'pageDescription' => __('site.page-description.fulfilled-prophecies'),
        ]);
    }

    public function galleries()
    {

        $galleries = Gallery::where('active', "1")->orderBy('id', 'desc')->paginate(8);

        return view('site.galleries', [
            'galleries' => $galleries,
            'pageTitle' => __('Photo Galleries'),
            'pageDescription' => __('site.page-description.galleries'),
        ]);
    }

    public function salvationPrayer()
    {

        return view('site.salvation-prayer', [
            'pageTitle' => __('Salvation Prayer'),
            'pageDescription' => __('site.page-description.salvation-prayer'),

        ]);
    }

    public function statementOfFaith()
    {
        $items = [
            'the_holy_bible',
            'the_trinity',
            'the_lord_jesus',
            'salvation',
            'the_lords_supper',
            'repentance_and_holiness',
            'the_holy_spirit',
            'the_church_of_christ',
            'the_rapture',
            'water_baptism',
            'preparing_the_way',
        ];

        return view('site.statement-of-faith', [
            'items' => $items,
            'pageTitle' => __('Statement of Faith'),
            'pageDescription' => __('site.page-description.statement-of-faith'),

        ]);
    }

    public function contacts()
    {
        return view('site.contacts', [
            'pageTitle' => __('Contacts'),
            'pageDescription' => __('site.page-description.contacts'),
        ]);
    }

    public function fellowships()
    {
        return view('site.fellowships', [
            'items' => config('sys.fellowships'),
            'pageTitle' => __('Worship Every Sunday'),
            'pageDescription' => __('site.page-description.home'),
        ]);
    }

    public function email(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'max:100'],
            'email' => ['email', 'required', 'max:100'],
            'subject' => ['required', 'max:100'],
            'message' => ['required'],
            'g-recaptcha-response' => ['required', 'recaptcha'],
        ], [
            'name.required' => __('The name field is required'),
            'email.required' => __('The email field is required'),
            'subject.required' => __('The subject field is required'),
            'message.required' => __('The message field is required'),
            'g-recaptcha-response.required' => __('The recaptcha field is required'),
            'g-recaptcha-response.recaptcha' => __('Failed Recaptcha Challenge. Are you a robot?'),
        ]);

        Notification::route('mail', env('MAIL_TO_ADDRESS', 'juliabruchwitzberlin@gmail.com'))
            ->notify(new General($request->name, $request->email, $request->subject, $request->message));

        return redirect('contacts')->with(['info' => __('site.messages.contact-thank-you')]);
    }
}
