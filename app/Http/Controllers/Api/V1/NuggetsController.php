<?php

namespace App\Http\Controllers\Api\V1;

use App\Nugget;
use App\Http\Controllers\Controller;
use App\Http\Resources\Nugget as NuggetResource;
use App\Http\Requests\Admin\StoreNuggetsRequest;
use App\Http\Requests\Admin\UpdateNuggetsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Traits\FileUploadTrait;


class NuggetsController extends Controller
{
    public function index()
    {
        return new NuggetResource(Nugget::with([])->get());
    }

    public function show($id)
    {
        if (Gate::denies('nugget_view')) {
            return abort(401);
        }

        $nugget = Nugget::with([])->findOrFail($id);

        return new NuggetResource($nugget);
    }

    public function images($id)
    {
        if (Gate::denies('nugget_view')) {
            return abort(401);
        }

        $nugget = Nugget::with([])->findOrFail($id);

        return NuggetResource::make($nugget)->additional(['images' => $nugget->media()->orderBy('id', 'desc')->paginate(10)]);
    }

    public function store(StoreNuggetsRequest $request)
    {
        if (Gate::denies('nugget_create')) {
            return abort(401);
        }

        $data = $request->all();
        $data['slug'] = $request->title;
        $nugget = Nugget::create($data);

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $key => $file) {
                $nugget->addMedia($file)->toMediaCollection('images');
            }
        }

        return (new NuggetResource($nugget))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateNuggetsRequest $request, $id)
    {
        if (Gate::denies('nugget_edit')) {
            return abort(401);
        }

        $nugget = Nugget::findOrFail($id);

        $nugget->fill($request->only(['title', 'description', 'video_link', 'order', 'show_on_front_page', 'active']))->save();

        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $key => $file) {
                $nugget->addMedia($file)->toMediaCollection('images');
            }
        }

        return (new NuggetResource($nugget))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('nugget_delete')) {
            return abort(401);
        }

        $nugget = Nugget::findOrFail($id);
        $nugget->delete();

        return response(null, 204);
    }
}
