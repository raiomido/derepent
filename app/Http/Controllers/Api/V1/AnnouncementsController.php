<?php

namespace App\Http\Controllers\Api\V1;

use App\Announcement;
use App\Http\Controllers\Controller;
use App\Http\Resources\Announcement as AnnouncementResource;
use App\Http\Requests\Admin\StoreAnnouncementsRequest;
use App\Http\Requests\Admin\UpdateAnnouncementsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;



class AnnouncementsController extends Controller
{
    public function index()
    {
        

        return new AnnouncementResource(Announcement::with([])->get());
    }

    public function show($id)
    {
        if (Gate::denies('announcement_view')) {
            return abort(401);
        }

        $announcement = Announcement::with([])->findOrFail($id);

        return new AnnouncementResource($announcement);
    }

    public function store(StoreAnnouncementsRequest $request)
    {
        if (Gate::denies('announcement_create')) {
            return abort(401);
        }

        $announcement = Announcement::create($request->all());
        
        

        return (new AnnouncementResource($announcement))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateAnnouncementsRequest $request, $id)
    {
        if (Gate::denies('announcement_edit')) {
            return abort(401);
        }

        $announcement = Announcement::findOrFail($id);
        $announcement->update($request->all());
        
        
        

        return (new AnnouncementResource($announcement))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('announcement_delete')) {
            return abort(401);
        }

        $announcement = Announcement::findOrFail($id);
        $announcement->delete();

        return response(null, 204);
    }
}
