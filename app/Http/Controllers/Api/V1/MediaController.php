<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;

class MediaController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param $model
     * @param Media $media
     * @return Response
     * @throws \Exception
     */
    public function destroy(Media $media, $model)
    {
        if (Gate::denies('gallery_delete')) {
            return abort(400, 'Permission Denied');
        }

        if (!$this->isOwner($model, $media)) {
            return abort(400, 'Media parent could not be determined');
        }

        $media->delete();

        return response(null, 204);
    }

    private function isOwner($model, Media $media) {
        $media->load(['model']);
        return $model == $media->model->id;
    }
}
