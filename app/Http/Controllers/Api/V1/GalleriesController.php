<?php

namespace App\Http\Controllers\Api\V1;

use App\Gallery;
use App\Http\Controllers\Controller;
use App\Http\Resources\Gallery as GalleryResource;
use App\Http\Requests\Admin\StoreGalleriesRequest;
use App\Http\Requests\Admin\UpdateGalleriesRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Traits\FileUploadTrait;


class GalleriesController extends Controller
{
    public function index()
    {
        

        return new GalleryResource(Gallery::with([])->get());
    }

    public function show($id)
    {
        if (Gate::denies('gallery_view')) {
            return abort(401);
        }

        $gallery = Gallery::with([])->findOrFail($id);

        return new GalleryResource($gallery);
    }

    public function store(StoreGalleriesRequest $request)
    {
        if (Gate::denies('gallery_create')) {
            return abort(401);
        }

        $gallery = Gallery::create($request->all());
        
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $key => $file) {
                $gallery->addMedia($file)->toMediaCollection('images');
            }
        }

        return (new GalleryResource($gallery))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateGalleriesRequest $request, $id)
    {
        if (Gate::denies('gallery_edit')) {
            return abort(401);
        }

        $gallery = Gallery::findOrFail($id);
        $gallery->update($request->all());
        
        $filesInfo = explode(',', $request->input('uploaded_images'));
        foreach ($gallery->getMedia('images') as $file) {
            if (! in_array($file->id, $filesInfo)) {
                $file->delete();
            }
        }
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $key => $file) {
                $gallery->addMedia($file)->toMediaCollection('images');
            }
        }

        return (new GalleryResource($gallery))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('gallery_delete')) {
            return abort(401);
        }

        $gallery = Gallery::findOrFail($id);
        $gallery->delete();

        return response(null, 204);
    }
}
