<?php

namespace App\Http\Controllers\Api\V1;

use App\Slider;
use App\Http\Controllers\Controller;
use App\Http\Resources\Slider as SliderResource;
use App\Http\Requests\Admin\StoreSlidersRequest;
use App\Http\Requests\Admin\UpdateSlidersRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Traits\FileUploadTrait;


class SlidersController extends Controller
{
    public function index()
    {
        

        return new SliderResource(Slider::with([])->get());
    }

    public function show($id)
    {
        if (Gate::denies('slider_view')) {
            return abort(401);
        }

        $slider = Slider::with([])->findOrFail($id);

        return new SliderResource($slider);
    }

    public function store(StoreSlidersRequest $request)
    {
        if (Gate::denies('slider_create')) {
            return abort(401);
        }

        $slider = Slider::create($request->all());
        
        if ($request->hasFile('image')) {
            $slider->addMedia($request->file('image'))->toMediaCollection('image');
        }

        return (new SliderResource($slider))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateSlidersRequest $request, $id)
    {
        if (Gate::denies('slider_edit')) {
            return abort(401);
        }

        $slider = Slider::findOrFail($id);
        $slider->update($request->all());
        
        if (! $request->input('image') && $slider->getFirstMedia('image')) {
            $slider->getFirstMedia('image')->delete();
        }
        if ($request->hasFile('image')) {
            $slider->addMedia($request->file('image'))->toMediaCollection('image');
        }

        return (new SliderResource($slider))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('slider_delete')) {
            return abort(401);
        }

        $slider = Slider::findOrFail($id);
        $slider->delete();

        return response(null, 204);
    }
}
