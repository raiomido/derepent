<?php

namespace App\Http\Controllers\Api\V1;

use App\Video;
use App\Http\Controllers\Controller;
use App\Http\Resources\Video as VideoResource;
use App\Http\Requests\Admin\StoreVideosRequest;
use App\Http\Requests\Admin\UpdateVideosRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Traits\FileUploadTrait;


class VideosController extends Controller
{
    public function index(Request $request)
    {
        $q = Video::with(['categories']);

        # Check if search term exists
        if ($s = $request->s)
            $q->search($s);

        return new VideoResource($q->latest()->paginate(80));
    }

    public function show($id)
    {
        if (Gate::denies('video_view')) {
            return abort(401);
        }

        $video = Video::with(['categories'])->findOrFail($id);

        return new VideoResource($video);
    }

    public function store(StoreVideosRequest $request)
    {
        if (Gate::denies('video_create')) {
            return abort(401);
        }

        $video = Video::create($request->all());
        $video->categories()->sync($request->input('categories', []));
        if ($request->hasFile('image')) {
            $video->addMedia($request->file('image'))->toMediaCollection('image');
        }

        return (new VideoResource($video))
            ->response()
            ->setStatusCode(201);
    }

    public function update(UpdateVideosRequest $request, $id)
    {
        if (Gate::denies('video_edit')) {
            return abort(401);
        }

        $video = Video::findOrFail($id);
        $video->update($request->all());
        $video->categories()->sync($request->input('categories', []));
        if (! $request->input('image') && $video->getFirstMedia('image')) {
            $video->getFirstMedia('image')->delete();
        }
        if ($request->hasFile('image')) {
            $video->addMedia($request->file('image'))->toMediaCollection('image');
        }

        return (new VideoResource($video))
            ->response()
            ->setStatusCode(202);
    }

    public function destroy($id)
    {
        if (Gate::denies('video_delete')) {
            return abort(401);
        }

        $video = Video::findOrFail($id);
        $video->delete();

        return response(null, 204);
    }
}
