<?php

namespace App\Http\Controllers;

use App\Nugget;
use Illuminate\Http\Request;

class NuggetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site.nuggets', [
            'nuggets' => Nugget::where('active', 1)->latest()->paginate(10),
            'pageTitle' => "Lerninhalte vom Weltweiten Abendgottesdienst 2020",
            'pageDescription' => 'Lerninhalte vom Weltweiten Abendgottesdienst',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param Nugget $nugget
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Nugget $nugget)
    {
        return view('site.nuggets-show', [
            'nugget_images' => $nugget->media()->orderBy('id', 'desc')->paginate(10),
            'pageTitle' => $nugget->title,
            'pageDescription' => $nugget->description,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
