<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class StreamController extends Controller
{
    public function live()
    {
        return view('site.live', [
            'hide_header' => true,
        ]);
    }
}
