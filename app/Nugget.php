<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Nugget
 *
 * @package App
 * @property string $title
 * @property text $description
 * @property string $video_link
 * @property integer $order
 * @property string $show_on_front_page
 * @property string $active
 */
class Nugget extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait {
        HasMediaTrait::addMedia as parentAddMedia;
    }

    protected $fillable = ['title', 'description', 'video_link', 'order', 'show_on_front_page', 'slug', 'active'];
    protected $appends = ['images'];

    public static function boot()
    {
        parent::boot();

        Nugget::observe(new \App\Observers\UserActionsObserver);
    }

    public function getRouteKeyName()
    {
        if(!request()->expectsJson()) {
            return 'slug';
        }
        return 'id';
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|nullable',
            'description' => 'max:65535|nullable',
            'images' => 'nullable',
            'images.*' => 'file|image|nullable',
            'video_link' => 'max:191|nullable',
            'order' => 'integer|max:4294967295|nullable',
            'show_on_front_page' => 'in:1,0|max:191|nullable',
            'active' => 'in:1,0|max:191|required'
        ];
    }

    public static function updateValidation($request)
    {
        return [
            'title' => 'max:191|nullable',
            'description' => 'max:65535|nullable',
            'images' => 'sometimes',
//            'images.*' => 'file|image|nullable',
            'video_link' => 'max:191|nullable',
            'order' => 'integer|max:4294967295|nullable',
            'show_on_front_page' => 'in:1,0|max:191|nullable',
            'active' => 'in:1,0|max:191|required'
        ];
    }

    public function setSlugAttribute($title)
    {
        if ($title) {
            $slug = Str::slug($title);
            do {
                $this->attributes['slug'] = $slug;
                $exists = Nugget::where('slug', $slug)->first();
                $slug = $slug.'-'.Str::random(3);
            } while ($exists);
        }
    }

    public function addMedia($file)
    {
        return $this->parentAddMedia($file)
            ->usingFileName($file->hashName());
    }

    public function getImagesAttribute()
    {
        return $this->getMedia('images')->keyBy('id');
    }

    public function getUploadedImagesAttribute()
    {
        return $this->getMedia('images')->keyBy('id');
    }

    /**
     * @return string
     */
    public function getImagesLinkAttribute()
    {
        $images = $this->getMedia('images');
        if (!count($images)) {
            return null;
        }
        $html = [];
        foreach ($images as $file) {
            $html[] = '<a href="' . $file->getUrl() . '" target="_blank">' . '<img width="200" src="'.$file->getUrl().'"' . ' alt="'.$file->file_name.'" ></a>';
        }

        return implode('<br/>', $html);
    }

    /**
     * @return string
     */
    public function getLatestImagesAttribute()
    {
        return $this->getMedia('images')->take('10')->keyBy('id');
    }

    public function scopeFrontPageNuggets(Builder $builder) {
        return $builder->where('show_on_front_page', 1)->where('active', 1)->orderBy('id', 'desc');
    }

    public function scopeOtherNuggets(Builder $builder) {
        return $builder->where('active', 1)->orderBy('id', 'desc')->orderBy('order', 'asc');
    }

    public function scopeLatestNuggets(Builder $builder) {
        return $builder->latest()->where('active', 1)->orderBy('id', 'desc')->orderBy('order', 'asc')->first();
    }
}
