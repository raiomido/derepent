<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class General extends Mailable
{
    use Queueable, SerializesModels;

    public $uname;
    public $uemail;
    public $usubject;
    public $umessage;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $subject, $message)
    {
        $this->uname = $name;
        $this->uemail = $email;
        $this->usubject = $subject;
        $this->umessage = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.general');
    }
}
