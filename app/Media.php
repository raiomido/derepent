<?php

namespace App;

use App\Observers\UserActionsObserver;
use Spatie\MediaLibrary\Models\Media as BaseMedia;

/**
 * Class Media
 * @property int $model_id
 * @property string $model_type
 * @property  string $collection_name
 * @property  string $name
 * @property  string $file_name
 * @property  string $mime_type
 * @property  string $disk
 * @property  int $size
 * @property  string $manipulations
 * @property  string $custom_properties
 * @property  string $responsive_images
 * @property  int $order_column
 * @package App
 */
class Media extends BaseMedia
{

    protected $table = 'media';

    public static function boot()
    {
        parent::boot();

        Media::observe(new UserActionsObserver());
    }

    public function getFullUrlAttribute()
    {
        return $this->getFullUrl();
    }

}
