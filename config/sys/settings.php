<?php
return [
	'recaptcha' => [
		'secret' => env('RECAPTCHA_SECRET'),
		'site' => env('RECAPTCHA_SITE'),
	],
    'website_info' => [
        'name' => env('APP_NAME'),
        'phone' => env('PRIMARY_PHONE'),
        'email' => env('PRIMARY_EMAIL'),
        'impressum' => 'Dienst der Buße und Heiligung e.V.',
    ]
];
