<?php
return [
    'links' => [
        'audio' => 'https://drive.google.com/folderview?id=0B2QCHH1KpwVueUp5WFpvN1hkVnc&usp=sharing',
        'radio' => 'https://jesusislordradio.info/',
        'main_website' => 'https://www.repentandpreparetheway.org/',
        'youtube' => 'https://www.youtube.com/channel/UCMnpNJH2PM1xEDifd_-JkmA',
        'prophetic_calling' => 'https://www.youtube.com/watch?v=fiBIpwbsolg',
        'video_teachings' => 'https://www.youtube.com/channel/UCVnvMpMVel0KMCqZM69XMbQ',
    ]
];
