$('#homeCarousel').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    margin: 0,
    responsiveClass: true,
    dots: false,
    stageOuterClass: 'owl-stage-outer h-100 d-flex align-items-center',
    stageClass: 'owl-stage d-flex align-items-center justify-content-center',
    responsive: {
        0: {
            items: 1,
            nav: false,
        },
        600: {
            items: 1,
            nav: true,
        },
        1000: {
            items: 1,
            nav: true,
        }
    }
});

$('#documentsCarousel').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    margin: 0,
    responsiveClass: true,
    dots: true,
    stageOuterClass: 'owl-stage-outer h-100 d-flex align-items-center',
    stageClass: 'owl-stage d-flex align-items-center justify-content-center',
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        600: {
            items: 1,
            nav: false
        },
        1000: {
            items: 1,
            nav: false
        }
    }
});

$('#visitationsCarousel').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    margin: 0,
    responsiveClass: true,
    dots: true,
    stageOuterClass: 'owl-stage-outer h-100 d-flex align-items-center',
    stageClass: 'owl-stage d-flex align-items-center justify-content-center',
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        600: {
            items: 1,
            nav: false
        },
        1000: {
            items: 1,
            nav: false
        }
    }
});

$('#generalVideosCarousel').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    margin: 0,
    responsiveClass: true,
    dots: true,
    stageOuterClass: 'owl-stage-outer h-100 d-flex align-items-center',
    stageClass: 'owl-stage d-flex align-items-center justify-content-center',
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        600: {
            items: 1,
            nav: false
        },
        1000: {
            items: 1,
            nav: false
        }
    }
});

$('#announcementsCarousel').owlCarousel({
    autoplay: true,
    autoplayHoverPause: true,
    loop: true,
    margin: 0,
    responsiveClass: true,
    dots: false,
    stageOuterClass: 'owl-stage-outer h-100 d-flex align-items-center',
    stageClass: 'owl-stage d-flex align-items-center justify-content-center',
    responsive: {
        0: {
            items: 1,
            nav: false
        },
        600: {
            items: 1,
            nav: false
        },
        1000: {
            items: 1,
            nav: false
        }
    }
});

$(".home-gallery-carousel").each(function () {
    $(this).owlCarousel({
        loop: true,
        autoplay: true,
        autoplayHoverPause: true,
        margin: 0,
        responsiveClass: true,
        dots: true,
        autoplayTimeout: getRandomTime(),
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
});

function getRandomTime() {
    var items = [
        5000,
        6000,
        4000,
        7000
    ];
    return items[Math.floor(Math.random()*items.length)]
}

$('.popup-video').fancybox();

let grid = $('.masonable-wrapper.grid')
if (grid.length) {
    let $grid = grid.masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        transitionDuration: 0
    });

    $grid.imagesLoaded().progress(function () {
        $grid.masonry();
        $('[data-fancybox="grid-gallery"]').fancybox({
            // Options will go here
        });
    });
}
