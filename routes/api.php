<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v1', 'middleware' => ['auth:api'], 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
    Route::post('change-password', 'ChangePasswordController@changePassword')->name('auth.change_password');
    Route::apiResource('rules', 'RulesController', ['only' => ['index']]);
    Route::apiResource('permissions', 'PermissionsController');
    Route::apiResource('roles', 'RolesController');
    Route::apiResource('users', 'UsersController');
    Route::apiResource('categories', 'CategoriesController');
    Route::apiResource('videos', 'VideosController');
    Route::apiResource('galleries', 'GalleriesController');
    Route::apiResource('documents', 'DocumentsController');
    Route::apiResource('user-actions', 'UserActionsController');
    Route::apiResource('sliders', 'SlidersController');
    Route::apiResource('announcements', 'AnnouncementsController');
    Route::apiResource('nuggets', 'NuggetsController');
    Route::get('nuggets/{nugget}/images', 'NuggetsController@images');
    Route::delete('media/{media}/i/{model}', 'MediaController@destroy')->name('media.destroy');
});
