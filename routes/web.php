<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');
Route::get('/test', 'TestController@index');
Route::get('/statement-of-faith', 'HomeController@statementOfFaith')->name('statement-of-faith');
Route::get('/contacts', 'HomeController@contacts')->name('contacts');
Route::get('/fellowships', 'HomeController@fellowships')->name('fellowships');
Route::post('/contacts', 'HomeController@email')->name('contacts.email');
Route::get('/salvation-prayer', 'HomeController@salvationPrayer')->name('salvation-prayer');
Route::get('/prophecies', 'HomeController@prophecies')->name('prophecies');
Route::get('/prophecies/fulfilled', 'HomeController@fulfilledProphecies')->name('fulfilled-prophecies');
Route::get('/stream/live', 'StreamController@live')->name('stream.live');

Route::resource('categories', 'CategoriesController')->only(['index', 'show']);
Route::resource('galleries', 'GalleriesController')->only(['index']);
Route::get('lernpunkte', 'NuggetsController@index')->name('nuggets.index');
Route::get('lernpunkte/{nugget}', 'NuggetsController@show')->name('nuggets.show');
Route::resource('slider-images', 'SlidersController')->only(['index']);

// Authentication Routes...
//Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
//Route::post('login', 'Auth\LoginController@login')->name('auth.login');
//Route::post('logout', 'Auth\LoginController@logout')->name('auth.logout');
//
//// Change Password Routes...
//Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
//Route::patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');
//
//// Password Reset Routes...
//Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
//Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
//Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Auth::routes();

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/', 'BackofficeController@index')->name('index');
    Route::get('/{any}', 'BackofficeController@index')->where('any', '.*');
});
